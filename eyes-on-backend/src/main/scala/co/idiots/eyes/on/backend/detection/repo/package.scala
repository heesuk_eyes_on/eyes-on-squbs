package co.idiots.eyes.on.backend.detection

import java.util.Date

import io.getquill.{MappedEncoding, PostgresJdbcContext, SnakeCase}
import org.joda.time.{DateTime, DateTimeZone}
import spray.httpx.PlayJsonSupport

package object repo extends UpsertDetection with GetDetection {

  lazy val dbContext = new PostgresJdbcContext[SnakeCase]("detection.manage.postgres") with PlayJsonSupport

  object SQLEntities {
    case class Detection(
        userId: Int,
        deviceType: String,
        deviceId: Int,
        datetime: DateTime,
        waterCount: Int,
        refreshCount: Int)
  }

  implicit val encodeDate: MappedEncoding[Date, DateTime] =
    MappedEncoding[Date, DateTime](new DateTime(_).withZone(DateTimeZone.UTC))
  implicit val decodeDate: MappedEncoding[DateTime, Date] = MappedEncoding[DateTime, Date](_.toDate)

}
