package co.idiots.eyes.on.backend.journal

import java.util.Date

import akka.util.Timeout
import co.idiots.eyes.on.message.Journal
import io.getquill.{MappedEncoding, PostgresJdbcContext, SnakeCase}
import org.joda.time.{DateTime, DateTimeZone}
import spray.httpx.PlayJsonSupport

import scala.concurrent.duration.SECONDS

package object repo {


  lazy val dbContext = new PostgresJdbcContext[SnakeCase]("journal.manage.postgres") with PlayJsonSupport

  object SQLEntities {
    case class Journal(
        journalId: Int,
        userId: Int,
        subject: String,
        content: String,
        weatherStatus: String,
        dailyMaxTemp: Double,
        dailyMinTemp: Double,
        dailyHumid: Double,
        datetime: DateTime,
        createdAt: DateTime = new DateTime(),
        updatedAt: DateTime = new DateTime())
  }

  implicit val encodeDate: MappedEncoding[Date, DateTime] =
    MappedEncoding[Date, DateTime](new DateTime(_).withZone(DateTimeZone.UTC))
  implicit val decodeDate: MappedEncoding[DateTime, Date] = MappedEncoding[DateTime, Date](_.toDate)

  def convertToJournalMsg(j: SQLEntities.Journal): Journal = {
    Journal(
      Some(j.journalId),
      j.userId,
      j.subject,
      j.content,
      j.weatherStatus,
      j.dailyMaxTemp,
      j.dailyMinTemp,
      j.dailyHumid,
      new DateTime(j.datetime).withZone(DateTimeZone.forID("Asia/Seoul")).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0)
    )
  }

}
