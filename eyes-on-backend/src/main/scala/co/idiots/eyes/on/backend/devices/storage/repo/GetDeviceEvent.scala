package co.idiots.eyes.on.backend.devices.storage.repo

import co.idiots.eyes.on.message.DeviceUUID
import com.mongodb.casbah.Imports._
import org.joda.time.{DateTime, DateTimeZone}
import play.api.libs.json.{JsArray, JsObject, Json}
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.{Future, blocking}

trait GetDeviceEvent {

  private def getJsonArray(list: Seq[JsObject]): JsArray = {
    list.foldLeft(JsArray())((acc, x) => acc ++ Json.arr(x))
  }

  private def createMongoDbObjectForFinding(deviceUUID: DeviceUUID, from: DateTime, to: DateTime): DBObject = {
    DBObject(
      "device_type" -> deviceUUID.`type`,
      "device_id" -> deviceUUID.id) ++ (
      "timestamp" $gte new DateTime(from).withZone(DateTimeZone.UTC).toDate $lt new DateTime(to).withZone(DateTimeZone.UTC).toDate)
  }

  def getDeviceEvent(deviceUUID: DeviceUUID, from: DateTime, to: DateTime, limit: Int, desc: Boolean): Future[JsArray] = {
    Future {
      blocking {
        val findObjects = collection.find(createMongoDbObjectForFinding(deviceUUID, from, to))
            .sort(MongoDBObject("timestamp" -> (if (desc) -1 else 1)))
            .limit(limit)
            .map(obj => Json.parse(obj.toString).as[JsObject])
        getJsonArray(findObjects.toVector)
      }
    }
  }
}
