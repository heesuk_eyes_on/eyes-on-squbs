package co.idiots.eyes.on.backend.weather

import akka.actor.{Actor, ActorLogging}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpMethods, HttpRequest}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import co.idiots.eyes.on.message.{InternalError, Weather}
import com.typesafe.config.ConfigFactory
import play.api.libs.json.Json

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

class WeatherManageActor extends Actor with ActorLogging {

  private val weatherUri: String = ConfigFactory.load().getString("weather.api.host")
  private val weatherApiKey: String = ConfigFactory.load().getString("weather.api.key")

  import context.system
  implicit val materializer = ActorMaterializer.create(system)

  override def receive: Receive = {
    //TODO: how about separate business logic?
    case Weather.GetCurrentDailyWeatherRequest() =>
      val requester = sender()
      val uri = weatherUri + "&appid=" + weatherApiKey
      val orchF = for {
        response <- Http().singleRequest(HttpRequest(method = HttpMethods.GET, uri = uri))
        weatherJs <- Unmarshal(response.entity).to[String].map { jsonStr =>
          Json.parse(jsonStr)
        }
        _ = Http().shutdownAllConnectionPools()
      } yield convertToWeather(weatherJs).get
      orchF onComplete {
        case Success(weather) =>
          requester ! weather
        case Failure(ex) => requester ! InternalError(ex.getMessage)
      }
  }
}