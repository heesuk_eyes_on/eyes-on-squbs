package co.idiots.eyes.on.backend.detection.repo

import java.util.Date

import co.idiots.eyes.on.message.{DeviceUUID, IsDetected}
import org.joda.time.{DateTime, DateTimeZone}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, blocking}

trait UpsertDetection {

  import dbContext._

  private def convertToDate(datetime: DateTime): DateTime =
    datetime.withZone(DateTimeZone.forID("Asia/Seoul")).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0)

  private def isExistDetectedEventOnTheDay(userId: Int, deviceUUID: DeviceUUID, dateTime: DateTime): Boolean = {
    dbContext.run(quote(query[SQLEntities.Detection].filter{ x =>
      x.userId == lift(userId) &&
      x.deviceType == lift(deviceUUID.`type`) &&
      x.deviceId == lift(deviceUUID.id) &&
      x.datetime == lift(dateTime)
    })).headOption match {
      case Some(_) => true
      case None => false
    }
  }

  private def incrementWaterCount(userId: Int, deviceUUID: DeviceUUID, dateTime: DateTime): RunActionResult = {
    println("incrementWaterCount In!")
    dbContext.run(quote(query[SQLEntities.Detection]
      .filter { x =>
        x.userId == lift(userId) &&
        x.deviceType == lift(deviceUUID.`type`) &&
        x.deviceId == lift(deviceUUID.id) &&
        x.datetime == lift(dateTime)
      }.update(d => d.waterCount -> (d.waterCount + 1))))
  }

  private def incrementRefreshCount(userId: Int, deviceUUID: DeviceUUID, dateTime: DateTime): RunActionResult = {
    println("incrementRefreshCount In!")
    dbContext.run(quote(query[SQLEntities.Detection]
      .filter { x =>
        x.userId == lift(userId) &&
          x.deviceType == lift(deviceUUID.`type`) &&
          x.deviceId == lift(deviceUUID.id) &&
          x.datetime == lift(dateTime)
      }.update(d => d.refreshCount -> (d.refreshCount + 1))))
  }

  private def incrementDetectionCountByDetected(userId: Int, deviceUUID: DeviceUUID, convertedDatetime: DateTime, detected: IsDetected) = {
    if(detected.waterDetection) incrementWaterCount(userId, deviceUUID, convertedDatetime)
    if(detected.refreshDetection) incrementRefreshCount(userId, deviceUUID, convertedDatetime)
  }

  private def insertNewDetection(userId: Int, deviceUUID: DeviceUUID, convertedDatetime: DateTime, detected: IsDetected) = {
    println("insertNewDetection In!")
    dbContext.run(quote(query[SQLEntities.Detection]
      .insert(lift(SQLEntities.Detection(userId, deviceUUID.`type`, deviceUUID.id, convertedDatetime, 0, 0)))))
    incrementDetectionCountByDetected(userId, deviceUUID, convertedDatetime, detected)
  }

  def upsertDetection(userId: Int, deviceUUID: DeviceUUID, datetime: DateTime, detected: IsDetected): Future[Unit] = {
    Future {
      blocking {
        val convertedDate = convertToDate(datetime)
        println("convertedDat upsert detection ! " + convertedDate)
        transaction {
          if(isExistDetectedEventOnTheDay(userId, deviceUUID, convertedDate)) {
            println("incrementDetectionCountByDetected in !")
            incrementDetectionCountByDetected(userId, deviceUUID, convertedDate, detected)
          } else insertNewDetection(userId, deviceUUID, convertedDate, detected)
        }
      }
    }
  }

}
