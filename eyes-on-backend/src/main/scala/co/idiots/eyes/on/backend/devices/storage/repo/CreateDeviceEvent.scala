package co.idiots.eyes.on.backend.devices.storage.repo

import co.idiots.eyes.on.message._
import com.mongodb.casbah.Imports.MongoDBObject
import org.joda.time.{DateTime, DateTimeZone}

import scala.concurrent.{Future, blocking}
import scala.concurrent.ExecutionContext.Implicits.global

trait CreateDeviceEvent {

  private def createMongoDbObject(payload: Payload) = {
    MongoDBObject(
      "timestamp" -> new DateTime().withZone(DateTimeZone.UTC).toDate,
      "device_type" -> payload.deviceUUID.`type`,
      "device_id" -> payload.deviceUUID.id,
      "sensor_value" ->
        MongoDBObject(
          "soil" -> payload.sensorValue.soil,
          "temp" -> payload.sensorValue.temp,
          "humid" -> payload.sensorValue.humid),
      "detection" ->
        MongoDBObject(
          "water_detection" -> payload.detection.waterDetection,
          "refresh_detection" -> payload.detection.refreshDetection))
  }

  def createDeviceEvent(deviceUUID: DeviceUUID, payload: Payload): Future[Unit] = {
    Future {
      blocking {
        collection.insert(createMongoDbObject(payload))
      }
    }
  }
}
