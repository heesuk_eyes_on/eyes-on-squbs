package co.idiots.eyes.on.backend.devices.manage

import java.util.Date

import io.getquill.{MappedEncoding, PostgresJdbcContext, SnakeCase}
import org.joda.time.{DateTime, DateTimeZone}
import spray.httpx.PlayJsonSupport

package object repo extends RegisterDevice
  with RegisterEndDevice
  with GetOwnership
  with GetUserDevices {

  lazy val dbContext = new PostgresJdbcContext[SnakeCase]("device.manage.postgres") with PlayJsonSupport

  object SQLEntities {
    case class Device(
        deviceType: String,
        deviceId: Int,
        deviceName: Option[String],
        secureCode: String,
        createdAt: DateTime = new DateTime(),
        updatedAt: DateTime = new DateTime())

    case class Ownership(
        userId: Int,
        deviceType: String,
        deviceId: Int,
        ownerType: String,
        createdAt: DateTime = new DateTime(),
        updatedAt: DateTime = new DateTime())

    case class HubGroupDevice(
        seedDeviceType: String,
        seedDeviceId: Int,
        hubDeviceId: Int,
        createdAt: DateTime = new DateTime())
  }

  implicit val encodeDate: MappedEncoding[Date, DateTime] =
    MappedEncoding[Date, DateTime](new DateTime(_).withZone(DateTimeZone.UTC))
  implicit val decodeDate: MappedEncoding[DateTime, Date] = MappedEncoding[DateTime, Date](_.toDate)
}