package co.idiots.eyes.on.backend.devices.manage.repo

import co.idiots.eyes.on.message.{DeviceUUID, UserDevice}

import scala.concurrent.{Future, blocking}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Failure

trait GetUserDevices {

  import dbContext._

  def getUserDevices(userId: Int, deviceType: String): Future[Vector[UserDevice]] = {
    println("get user devices")
    Future {
      blocking {
        run(quote(query[SQLEntities.Ownership]
          .filter(x => x.userId == lift(userId) &&
          x.deviceType == lift(deviceType))))
      }.map(ownershipDevice =>
        UserDevice(
          ownershipDevice.userId,
          DeviceUUID(ownershipDevice.deviceType, ownershipDevice.deviceId))).toVector
    } recover {
      case ex => throw ex
    }
  }
}
