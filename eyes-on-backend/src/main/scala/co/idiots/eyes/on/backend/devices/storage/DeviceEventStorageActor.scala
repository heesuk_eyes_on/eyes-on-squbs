package co.idiots.eyes.on.backend.devices.storage

import akka.actor.{Actor, ActorLogging}
import co.idiots.eyes.on.message._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

class DeviceEventStorageActor extends Actor with ActorLogging {

  override def receive: Receive = {
    case DeviceEvent.CreateRequest(deviceUUID, payload) =>
      val requester = sender()
      log.info("DeviceEvent.Create Request In")
      repo.createDeviceEvent(deviceUUID, payload) onComplete {
        case Success(_) => requester ! Empty()
        case Failure(ex) => requester ! InternalError(ex.getMessage)
      }

    case DeviceEvent.GetRequest(deviceUUID, from, to, limit, desc) =>
      val requester = sender()
      repo.getDeviceEvent(deviceUUID, from, to, limit, desc) onComplete {
        case Success(events) => requester ! DeviceEvent(events)
        case Failure(ex) => requester ! InternalError(ex.getMessage)
      }
  }

}
