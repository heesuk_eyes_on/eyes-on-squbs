package co.idiots.eyes.on.backend.devices.manage.repo

import co.idiots.eyes.on.backend.devices.manage.repo.SQLEntities.{HubGroupDevice, Ownership}
import co.idiots.eyes.on.message.{DeviceUUID, Unauthorized}

import scala.concurrent.{Future, blocking}
import scala.concurrent.ExecutionContext.Implicits.global

trait RegisterEndDevice {

  import dbContext._

  def registerEndDevice(ownerUserId: Int, hubDevice: DeviceUUID, seedDevice: DeviceUUID, secureCode: String): Future[Unit] = {
    def checkDeviceSecureCode(deviceUUID: DeviceUUID) = {
      run(quote(query[SQLEntities.Device].filter{ x =>
        x.deviceType == lift(deviceUUID.`type`) &&
          x.deviceId == lift(deviceUUID.id) &&
          x.secureCode == lift(secureCode)
      })).headOption
    }
    def createHubGroupDeviceQ = {
      run(quote(query[SQLEntities.HubGroupDevice].insert(
        lift(
          HubGroupDevice(
            seedDevice.`type`,
            seedDevice.id,
            hubDevice.id)))))
    }
    def registerOwnershipForSeed = {
      run(quote(query[SQLEntities.Ownership].insert(
        lift(
          Ownership(
            ownerUserId,
            seedDevice.`type`,
            seedDevice.id,
            "admin"))
      )))
    }

    Future {
      blocking {
        transaction {
          checkDeviceSecureCode(seedDevice) match {
            case Some(_) =>
              createHubGroupDeviceQ
              registerOwnershipForSeed
            case None =>
              throw Unauthorized("you did not match secure code for seed device")
          }
        }
      }
    }

  }
}
