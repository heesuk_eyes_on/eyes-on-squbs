package co.idiots.eyes.on.backend.devices.manage


import akka.actor.{Actor, ActorLogging}
import co.idiots.eyes.on.message._

import scala.util.{Failure, Success}

/**
  * Created by heesuk on 2017. 5. 14..
  */

class DeviceManageActor extends Actor with ActorLogging {

  import context.dispatcher

  override def receive: Receive = {
    case Device.RegisterRequest(userId, hubDevice, secureCode) =>
      val requester = sender()
      repo.registerDevice(userId, hubDevice, secureCode) onComplete {
        case Success(_) => requester ! Empty()
        case Failure(ex) => requester ! InternalError(ex.getMessage)
      }

    case Device.RegisterEndRequest(ownerUserId, hubDevice, seedDevice, secureCode) =>
      val requester = sender()
      repo.registerEndDevice(ownerUserId, hubDevice, seedDevice, secureCode) onComplete {
        case Success(_) => requester ! Empty()
        case Failure(ex) => requester ! InternalError(ex.getMessage)
      }

    case Ownership.GetRequest(deviceUUID) =>
      val requester = sender()
      log.info("ownership getRequest in!")
      repo.getOwnership(deviceUUID) onComplete {
        case Success(ownership) => requester ! ownership
        case Failure(ex) => requester ! InternalError(ex.getMessage)
      }

    case UserDevices.GetRequest(userId, deviceType) =>
      val requester = sender()
      repo.getUserDevices(userId, deviceType) onComplete {
        case Success(userDevices) =>
          println("userdevice getRequest : " + userDevices)
          requester ! UserDevices(userDevices)
        case Failure(ex) =>
          println("error! :" + ex.getMessage)
          requester ! InternalError(ex.getMessage)
      }
  }

}
