package co.idiots.eyes.on.backend.detection

import akka.actor.{Actor, ActorLogging}
import co.idiots.eyes.on.message.{Detection, Empty, InternalError}

import scala.util.{Failure, Success}
import scala.concurrent.ExecutionContext.Implicits.global

class DetectionManageActor extends Actor with ActorLogging {

  override def receive: Receive = {
    case Detection.CreateRequest(userId, deviceUUID, datetime, detected) =>
      val requester = sender()
      log.info("detection.createRequest in!!")
      repo.upsertDetection(userId, deviceUUID, datetime, detected) onComplete {
        case Success(_) => requester ! Empty()
        case Failure(ex) => requester ! InternalError(ex.getMessage)
      }

    case Detection.GetRequest(userDevice, datetime) =>
      val requester = sender()
      log.info("detection.GetRequest in!!")
      repo.getDetection(userDevice, datetime) onComplete {
        case Success(detection) => requester ! detection
        case Failure(ex) => requester ! InternalError(ex.getMessage)
      }
  }
}
