package co.idiots.eyes.on.backend

import java.util
import java.util.UUID

import akka.actor.{Actor, ActorLogging}
import co.idiots.eyes.on.message._
import com.auth0.jwt.JWTSigner
import com.github.t3hnar.bcrypt._
import com.typesafe.config.ConfigFactory
import io.getquill.{PostgresJdbcContext, SnakeCase}
import org.joda.time.DateTime
import spray.httpx.PlayJsonSupport

import scala.collection.JavaConversions.mapAsJavaMap
import scala.concurrent.{Future, blocking}
import scala.util.{Failure, Success}

/**
  * Created by heesuk on 2017. 5. 14..
  */
private[this] object UserManagementActor {
  object SQLEntities {
    case class DeviceOwnership(
        ownerType: String,
        createdAt: DateTime,
        updatedAt: DateTime,
        deviceId: Int,
        userId: Int)
    case class `public.user`(
        id: Int,
        phoneNumber: String,
        email: Option[String],
        emailConfirmed: Option[Boolean] = None,
        emailCode: Option[String] = None,
        password: String,
        passwordCode: String,
        firstName: Option[String] = None,
        lastName: Option[String] = None,
        dobYear: Option[Int] = None,
        dobMonth: Option[Int] = None,
        dobDay: Option[Int] = None,
        gender: Option[String] = None,
        job: Option[String] = None)
  }
  lazy val dbContext = new PostgresJdbcContext[SnakeCase]("user.management.actor.postgres") with PlayJsonSupport
}

class UserManagementActor extends Actor with ActorLogging {
  import UserManagementActor._
  import context.dispatcher
  import dbContext._

  private val apiServiceJwtSecret = ConfigFactory.load().getString("api.service.jwt.secret")

  private def userAuthToken(userId: Int): String = {
    val claims = mapAsJavaMap(Map("type" -> "access", "user_id" -> userId)).asInstanceOf[util.Map[String, AnyRef]]
    new JWTSigner(apiServiceJwtSecret).sign(claims)
  }

  override def receive = {
    case User.CreateRequest(phoneNumber, password, emailO, firstName, lastName, dobYearO, dobMonthO, dobDayO, genderO, jobO) =>
      val requester = sender()
      log.info("user create in!!!")
      val passwordCode = UUID.randomUUID().toString
      val userCreateF = Future {
        blocking {
          run(quote(query[SQLEntities.`public.user`]
            .insert(
              _.phoneNumber -> lift(phoneNumber),
              _.password -> lift(password.bcrypt),
              _.passwordCode -> lift(passwordCode),
              _.email -> lift(emailO),
              _.firstName -> lift(firstName),
              _.lastName -> lift(lastName),
              _.dobYear -> lift(dobYearO),
              _.dobMonth -> lift(dobMonthO),
              _.dobDay -> lift(dobDayO),
              _.gender -> lift(genderO),
              _.job -> lift(jobO))))
        }
      }
      userCreateF onComplete {
        case Success(_) =>
          log.info("user create empty!!!!")
          requester ! Empty()
        case Failure(ex) => requester ! InternalError(ex.getMessage)
      }

    case User.GetRequest(userId) =>
      val requester = sender()
      val userInfoF = Future {
        blocking {
          run(quote(query[SQLEntities.`public.user`].filter(x => x.id == lift(userId)))).headOption
        }
      }
      userInfoF onComplete {
        case Success(Some(u)) =>
          requester ! User(
            u.id,
            u.phoneNumber,
            u.email,
            u.emailConfirmed,
            u.emailCode,
            u.firstName,
            u.lastName,
            u.dobYear,
            u.dobMonth,
            u.dobDay,
            u.gender,
            u.job)
        case Success(None) => requester ! NotFound("user exist on eyes-on. please check `user_id`")
        case Failure(ex) => requester ! InternalError(ex.getMessage)
      }

    case Users.GetRequest() =>
      val requester = sender()
      def getUsersF = {
        Future {
          blocking {
            run(quote(query[SQLEntities.`public.user`]))
          }.toVector
        }
      }
      getUsersF onComplete {
        case Success(users) =>
          requester ! Users(users.map { u =>
            User(
              u.id,
              u.phoneNumber,
              u.email,
              u.emailConfirmed,
              u.emailCode,
              u.firstName,
              u.lastName,
              u.dobYear,
              u.dobMonth,
              u.dobDay,
              u.gender,
              u.job)
          })
        case Failure(ex) => requester ! InternalError(ex.getMessage)
      }

    case UserLogin.GetRequest(phoneNumber, password) =>
      val requester = sender()
      val loginF = Future {
        blocking {
          run(quote(query[SQLEntities.`public.user`].filter(x => x.phoneNumber == lift(phoneNumber)))).headOption
        }
      }
      loginF onComplete {
        case Success(Some(u)) if password.isBcrypted(u.password) => requester ! UserLogin(u.id, userAuthToken(u.id))
        case Success(None) => requester ! Unauthorized("invalid `phoneNumber` or `password`. please check your account")
        case Failure(ex) => requester ! InternalError(ex.getMessage)
      }
  }
}
