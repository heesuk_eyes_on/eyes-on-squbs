package co.idiots.eyes.on.backend.journal

import akka.actor.{Actor, ActorLogging, ActorSystem}
import co.idiots.eyes.on.message.Journal
import com.typesafe.akka.extension.quartz.QuartzSchedulerExtension


trait JournalScheduler extends Actor with ActorLogging {

  implicit def system: ActorSystem

  log.info("Register Journal Scheduler.")
  QuartzSchedulerExtension(system).schedule("Every12PMCreateAllUserJournal", self, Journal.CreateAllUserDailyJournal)
}
