package co.idiots.eyes.on.backend

import co.idiots.eyes.on.message.Weather
import play.api.libs.json.{JsResult, JsValue}

package object weather {

  def convertToWeather(weatherJs: JsValue): JsResult[Weather] = {
    val weatherList = (weatherJs \ "list").head
    val weather = (weatherList \ "weather").head
    for {
      dailyMaxTemp <- (weatherList \ "temp" \ "max").validate[Double]
      dailyMinTemp <- (weatherList \ "temp" \ "min").validate[Double]
      dailyHumid <- (weatherList \ "humidity").validate[Int]
      datetime <- (weatherList \ "dt").validate[Int]
      status <- (weather \ "main").validate[String]
    } yield Weather(dailyMaxTemp, dailyMinTemp, dailyHumid, datetime, status)
  }

}
