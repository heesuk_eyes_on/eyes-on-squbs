package co.idiots.eyes.on.backend.journal

import akka.actor.{Actor, ActorLogging, ActorSystem}
import akka.util.Timeout
import co.idiots.eyes.on.backend.journal.repo.{CreateAllUserDailyJournal, GetUserDailyJournals, GetUserSpecificDailyJournal}
import co.idiots.eyes.on.message.{InternalError, Journal, Journals}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.SECONDS
import scala.util.{Failure, Success}

class JournalManageActor extends Actor
  with ActorLogging
  with JournalScheduler
  with CreateAllUserDailyJournal
  with GetUserSpecificDailyJournal
  with GetUserDailyJournals {

  override def system: ActorSystem = context.system
  override def akkaTimeout: Timeout = Timeout(3, SECONDS)

  override def receive: Receive = {
    case Journal.CreateAllUserDailyJournal =>
      log.info("call journal.CreateAllUserDailyJournal")
      createAllUserDailyJournal

    case Journal.GetRequest(journalId) =>
      val requester = sender()
      log.info("journal.GetRequest in!")
      getUserSpecificDailyJournal(journalId) onComplete {
        case Success(journal) => requester ! journal
        case Failure(ex) => requester ! InternalError(ex.getMessage)
      }

    case Journals.GetRequest(userId, from, to) =>
      val requester = sender()
      log.info("journals.GetRequest in!")
      getUserDailyJournals(userId, from, to) onComplete {
        case Success(journals) => requester ! Journals(journals)
        case Failure(ex) => requester ! InternalError(ex.getMessage)
      }
  }
}
