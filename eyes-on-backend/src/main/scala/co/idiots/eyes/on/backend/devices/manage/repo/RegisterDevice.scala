package co.idiots.eyes.on.backend.devices.manage.repo

import co.idiots.eyes.on.backend.devices.manage.repo.SQLEntities.Ownership
import co.idiots.eyes.on.message.{DeviceUUID, Unauthorized}

import scala.concurrent.{Future, blocking}
import scala.concurrent.ExecutionContext.Implicits.global

trait RegisterDevice {

  import dbContext._

  type DeviceToken = String

  def registerDevice(userId: Int, hubDevice: DeviceUUID, secureCode: String): Future[Unit] = {
    def checkDeviceSecureCode = {
      run(quote(query[SQLEntities.Device].filter{ x =>
        x.deviceType == lift(hubDevice.`type`) &&
        x.deviceId == lift(hubDevice.id) &&
        x.secureCode == lift(secureCode)
      })).headOption
    }
    def registerOwnership =
      run(quote(query[SQLEntities.Ownership].insert(lift(Ownership(userId, hubDevice.`type`, hubDevice.id, "admin")))))

    Future {
      blocking {
        transaction {
          checkDeviceSecureCode match {
            case Some(_) =>
              registerOwnership
            case None =>
              throw Unauthorized("you have not authorization for device")
          }
        }
      }
    }
  }
}