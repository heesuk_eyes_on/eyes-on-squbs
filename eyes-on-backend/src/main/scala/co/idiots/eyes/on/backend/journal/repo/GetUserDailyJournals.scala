package co.idiots.eyes.on.backend.journal.repo

import co.idiots.eyes.on.message.Journal
import org.joda.time.DateTime

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, blocking}

trait GetUserDailyJournals {

  import dbContext._

  def getUserDailyJournals(userId: Int, from: DateTime, to: DateTime): Future[Vector[Journal]] = {
    val (fromTimeQ, toTimeQ) = (quote(infix"datetime >= ${lift(from)}".as[Boolean]), quote(infix"datetime < ${lift(to)}".as[Boolean]))
    val getUserDailyJournalsQ = quote {
      query[SQLEntities.Journal]
        .filter(x => x.userId == lift(userId) && fromTimeQ && toTimeQ)
        .sortBy(_.datetime)(Ord.desc)
    }

    Future {
      blocking {
        run(getUserDailyJournalsQ)
      }.map(j => convertToJournalMsg(j)).toVector
    }
  }
}
