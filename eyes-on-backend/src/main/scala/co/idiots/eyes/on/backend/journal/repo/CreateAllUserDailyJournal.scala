package co.idiots.eyes.on.backend.journal.repo

import akka.actor.ActorSystem
import akka.util.Timeout
import co.idiots.eyes.on.message.{User, Users, Weather}
import co.idiots.eyes.on.util.actorregistry.ActorLookup
import org.joda.time.{DateTime, DateTimeZone}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, blocking}

trait CreateAllUserDailyJournal {

  import dbContext._
  implicit def system: ActorSystem
  implicit def akkaTimeout: Timeout

  private val defaultSubject = "제목을 입력하세요."
  private val defaultContent = "내용을 입력하세요."

  private def getAllUser: Future[Vector[User]] = (ActorLookup[Users] ? Users.GetRequest()).map(_.data)

  //TODO: liftQuery(journals.toList).foreach(j => query[SQLEntities.Journal].insert(j)) batch job으로 변경해야함.
  private def createUserDailyJournal(user: User, currDailyWeather: Weather) = {
    run(quote(query[SQLEntities.Journal].insert(
      _.userId -> lift(user.id),
      _.subject -> lift(defaultSubject),
      _.content-> lift(defaultContent),
      _.weatherStatus -> lift(currDailyWeather.status),
      _.dailyMaxTemp -> lift(currDailyWeather.dailyMaxTemp),
      _.dailyMinTemp -> lift(currDailyWeather.dailyMinTemp),
      _.dailyHumid -> lift(currDailyWeather.dailyHumid)
      )))
  }

  def createAllUserDailyJournal: Future[Unit] = {
    for {
      currentDailyWeather <- ActorLookup[Weather] ? Weather.GetCurrentDailyWeatherRequest() //korea의 current weather을 가져온다.
      users <- getAllUser
      _ <- Future {
        blocking {
          transaction {
            users.map(u => createUserDailyJournal(u, currentDailyWeather))
          }
        }
      } recover {
        case ex => throw ex
      }
    } yield ()
  }
}
