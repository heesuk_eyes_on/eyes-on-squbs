package co.idiots.eyes.on.backend.journal.repo

import co.idiots.eyes.on.message.{Journal, NotFound}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, blocking}

trait GetUserSpecificDailyJournal {

  import dbContext._

  def getUserSpecificDailyJournal(journalId: Int): Future[Journal] = {
    Future {
      blocking {
        run(quote(query[SQLEntities.Journal].filter(x => x.journalId == lift(journalId)))).headOption match {
          case Some(j) => convertToJournalMsg(j)
          case None => throw NotFound("not found specific daily journal")
        }
      }
    }
  }
}
