package co.idiots.eyes.on.backend.devices.manage.repo

import co.idiots.eyes.on.message.{DeviceUUID, NotFound, Ownership}

import scala.concurrent.{Future, blocking}
import scala.concurrent.ExecutionContext.Implicits.global

trait GetOwnership {

  import dbContext._

  def getOwnership(deviceUUID: DeviceUUID): Future[Ownership] = {
    Future {
      blocking {
        run(quote(query[SQLEntities.Ownership]
          .filter(x => x.deviceType == lift(deviceUUID.`type`) && x.deviceId == lift(deviceUUID.id))))
          .map(o => Ownership(o.userId, DeviceUUID(o.deviceType, o.deviceId), o.ownerType)).headOption match {
          case Some(ownership) => ownership
          case None => throw NotFound("not found device owner")
        }
      }
    }
  }

}
