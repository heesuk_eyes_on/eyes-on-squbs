package co.idiots.eyes.on.backend.detection.repo

import co.idiots.eyes.on.message.{Detection, DeviceUUID, NotFound, UserDevice}
import org.joda.time.DateTime

import scala.concurrent.{Future, blocking}
import scala.concurrent.ExecutionContext.Implicits.global

trait GetDetection {

  import dbContext._

  def getDetection(userDevice: UserDevice, datetime: DateTime): Future[Detection] = {
    Future {
      blocking {
        run(quote(query[SQLEntities.Detection].filter(x =>
          x.userId == lift(userDevice.userId) &&
          x.deviceType == lift(userDevice.deviceUUID.`type`) &&
          x.deviceId == lift(userDevice.deviceUUID.id) &&
          x.datetime == lift(datetime)))).headOption match {
          case Some(d) => Detection(d.userId, DeviceUUID(d.deviceType, d.deviceId), d.waterCount, d.refreshCount, d.datetime)
          case None => Detection(userDevice.deviceUUID.id, userDevice.deviceUUID, 0, 0, datetime)
        }
      }
    }
  }
}
