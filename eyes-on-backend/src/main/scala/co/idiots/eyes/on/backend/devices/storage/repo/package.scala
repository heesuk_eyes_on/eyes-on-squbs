package co.idiots.eyes.on.backend.devices.storage

import com.mongodb.casbah.Imports.{MongoClient, MongoClientURI}
import com.typesafe.config.ConfigFactory
import play.api.libs.json.JsObject

package object repo extends CreateDeviceEvent with GetDeviceEvent {

  val conf = ConfigFactory.load().getConfig("device.storage.actor.mongo.db")
  val uri = MongoClientURI(conf.getString("url"))
  val mongoClient = MongoClient(uri)
  val db = mongoClient("sensor")
  val collection = db("sensor")

  case class DeviceStorageQuery(data: JsObject)
}
