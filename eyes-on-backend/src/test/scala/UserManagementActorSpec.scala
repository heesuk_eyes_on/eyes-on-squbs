package co.idiots.eyes.on.backend

import java.time.LocalDateTime

import co.idiots.eyes.on.util.TestKit
import co.idiots.eyes.on.util.actorregistry.ActorLookup
import org.joda.time.DateTime
import org.scalatest.{BeforeAndAfterEach, FlatSpecLike, Matchers}
import org.scalatest._
import org.squbs.testkit.Timeouts._
import org.joda.time.DateTime._
import co.idiots.eyes.on.message.{Empty, User, UserLogin, Users}

import scala.concurrent.Await


/**
  * Created by heesuk on 2017. 5. 14..
  */
class UserManagementActorSpec
  extends TestKit(true)
  with FlatSpecLike
  with BeforeAndAfterEach
  with Matchers{

  import UserManagementActor._
  import dbContext._

  private val createdAt = LocalDateTime.now()
  private val userId = 0
  private val otherUserId = 1
  private val dateTime = new DateTime()
  private val password = "foo"
  private val passwordByBcrypt = "$2a$10$Y/ngrJ13ZolX4/5MHjnWMeNx4xC4rGdaZ6YAK9onvJXUkklHWW1Si"

  override def beforeEach(): Unit = {
    super.beforeEach()
    executeAction(
      """
        |DROP SEQUENCE IF EXISTS public.user_id_seq CASCADE;
        |DROP TABLE IF EXISTS public."user" CASCADE;
        |
        |CREATE SEQUENCE public.user_id_seq
        |  INCREMENT 1
        |  MINVALUE 1
        |  MAXVALUE 9223372036854775807
        |  START 1
        |  CACHE 1;
        |ALTER TABLE public.user_id_seq
        |  OWNER TO root;
        |
        |CREATE TABLE public."user"
        |(
        |  id integer NOT NULL DEFAULT nextval('user_id_seq'::regclass),
        |  phone_number character varying(255) NOT NULL,
        |  email character varying(255),
        |  email_confirmed boolean DEFAULT false,
        |  email_code character varying(255) DEFAULT ''::character varying,
        |  password character varying(255),
        |  password_code character varying(255) NOT NULL,
        |  password_code_issued_at timestamp with time zone DEFAULT now(),
        |  first_name character varying(255),
        |  last_name character varying(255),
        |  dob_year integer,
        |  dob_month integer,
        |  dob_day integer,
        |  gender character varying(255),
        |  job character varying(255),
        |  created_at timestamp with time zone NOT NULL DEFAULT now(),
        |  updated_at timestamp with time zone NOT NULL DEFAULT now(),
        |  CONSTRAINT user_pkey PRIMARY KEY (id),
        |  CONSTRAINT user_email_key UNIQUE (email),
        |  CONSTRAINT user_phone_number_key UNIQUE (phone_number)
        |)
        |WITH (
        |  OIDS=FALSE
        |);
        |ALTER TABLE public."user"
        |  OWNER TO root;
      """.stripMargin)
    executeAction(
      s"""
         |INSERT INTO public."user" (phone_number, password, password_code, email, first_name, last_name, dob_year, dob_month, dob_day, gender, email_confirmed, created_at, updated_at, email_code) VALUES('01098765432', '$passwordByBcrypt', 'passwordCode', 'foo@gmail.com', 'heesuk', 'ahn', 1990, 5, 18, 'male', true, '${dateTime.toDate}', '${dateTime.toDate}', 'foo');
         |INSERT INTO public."user" (phone_number, password, password_code, email, first_name, last_name, dob_year, dob_month, dob_day, gender, email_confirmed, created_at, updated_at, email_code) VALUES(e'01012345678', '$passwordByBcrypt', 'passwordCode', 'bar@gmail.com', 'sangwoo', 'kim', 1990, 6, 21, 'male', true, '${dateTime.toDate}', '${dateTime.toDate}', 'bar');
      """.stripMargin)
  }

  "User.CreateRequest" should "create new user" in {
    Await.result(ActorLookup[Empty] ? User.CreateRequest("01099704855", "foo") , awaitMax) shouldBe Empty()
    dbContext.run(quote(query[SQLEntities.`public.user`].filter(x => x.id == 3))).head should matchPattern {
      case SQLEntities.`public.user`(3, "01099704855", None, Some(false), _, _, _, None, None, None, None, None, None, None) =>
    }
  }

  "User.GetRequest" should "retrieve user information" in {
    Await.result(ActorLookup[User] ?
      User.GetRequest(1), awaitMax) shouldBe User(
      1,
      "01098765432",
      Some("foo@gmail.com"),
      Some(true),
      Some("foo"),
      Some("heesuk"),
      Some("ahn"),
      Some(1990),
      Some(5),
      Some(18),
      Some("male"),
      None)
  }

  "Users.GetRequest" should "return all user information" in {
    Await.result(ActorLookup[Users] ? Users.GetRequest(), awaitMax) shouldBe
      Users(
        Vector(
          User(
            1,
            "01098765432",
            Some("foo@gmail.com"),
            Some(true),
            Some("foo"),
            Some("heesuk"),
            Some("ahn"),
            Some(1990),
            Some(5),
            Some(18),
            Some("male"),
            None),
          User(
            2,
            "01012345678",
            Some("bar@gmail.com"),
            Some(true),
            Some("bar"),
            Some("sangwoo"),
            Some("kim"),
            Some(1990),
            Some(6),
            Some(21),
            Some("male"),
            None)))
  }

  "UserLogin.GetRequest" should "confirm user account whether exist or not" in {
    Await.result(ActorLookup[UserLogin] ? UserLogin.GetRequest("01098765432", password), awaitMax) should matchPattern {
      case login =>
    }
  }
}
