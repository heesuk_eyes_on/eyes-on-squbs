package co.idiots.eyes.on.backend.journal.repo

import org.scalatest.{BeforeAndAfterEach, Suite}

import scala.concurrent.ExecutionContext

trait PostgresTestKit extends BeforeAndAfterEach { self: Suite =>

  import dbContext._

  implicit val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global

  val dbInsertQToBeforeEach = new StringBuilder

  val baseDatabase: String =
    """
      |DROP TABLE IF EXISTS public.journal CASCADE;
      |
      |CREATE TABLE journal
      |(
      |  journal_id serial,
      |  user_id integer NOT NULL,
      |  subject character varying(255) NOT NULL,
      |  content character varying(255) NOT NULL,
      |  weather_status character varying(255) NOT NULL,
      |  daily_max_temp double precision NOT NULL,
      |  daily_min_temp double precision NOT NULL,
      |  daily_humid double precision NOT NULL,
      |  datetime timestamp DEFAULT now() NOT NULL,
      |  created_at timestamp DEFAULT now() NOT NULL,
      |  updated_at timestamp DEFAULT now() NOT NULL,
      |  CONSTRAINT user_id_journal_id_datetime PRIMARY KEY (user_id, journal_id, datetime)
      |)
      |WITH (
      |  OIDS=FALSE
      |);
      |ALTER TABLE journal
      |  OWNER TO root;
    """.stripMargin

  dbInsertQToBeforeEach.append(baseDatabase)

  override def beforeEach(): Unit = {
    super.beforeEach()

    executeAction(
      dbInsertQToBeforeEach.toString
    )
  }

  override def afterEach(): Unit = {
    dbContext.executeAction(
      """
        |DROP TABLE IF EXISTS public.journal CASCADE;
      """.stripMargin)
    super.afterEach()
  }

}

