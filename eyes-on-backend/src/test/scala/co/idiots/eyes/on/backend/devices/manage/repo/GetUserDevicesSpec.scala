package co.idiots.eyes.on.backend.devices.manage.repo

import co.idiots.eyes.on.message.{DeviceUUID, UserDevice}

class GetUserDevicesSpec extends PostgresTestSupportKit with GetUserDevices {

  val userId = 1
  val deviceType = "seed"
  val deviceId = 1

  dbInsertQToBeforeEach.append(
    s"""
      |INSERT INTO device (device_type, device_id, secure_code) VALUES ('$deviceType', $deviceId, 'code');
      |INSERT INTO ownership (user_id, device_type, device_id, owner_type) VALUES ($userId, '$deviceType', $deviceId, 'admin');
    """.stripMargin)

  "getUserDevices" should "return user`s devices list" in {
    getUserDevices(userId, deviceType).await shouldBe Vector(UserDevice(userId, DeviceUUID(deviceType, deviceId)))
  }
}