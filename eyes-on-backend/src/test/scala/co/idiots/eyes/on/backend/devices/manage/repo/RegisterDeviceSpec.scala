package co.idiots.eyes.on.backend.devices.manage.repo

import co.idiots.eyes.on.message.DeviceUUID

class RegisterDeviceSpec() extends PostgresTestSupportKit with RegisterDevice {

  import dbContext._

  val ownerUserId = 1
  val deviceType = "hub"
  val deviceId = 1
  val hubDevice = DeviceUUID(deviceType, deviceId)
  val secureCode = "foo"

  dbInsertQToBeforeEach.append(s"INSERT INTO device (device_type, device_id, secure_code) VALUES('$deviceType', $deviceId, '$secureCode');")

  private object CheckDBContent {
    lazy val isExistOwnership: Unit = dbContext.run(
      quote(
        query[SQLEntities.Ownership]
          .filter(x => x.userId == lift(ownerUserId)))).head should matchPattern {
      case SQLEntities.Ownership(`ownerUserId`, `deviceType`, `deviceId`, "admin", _, _)
        if `ownerUserId` == ownerUserId &&
          `deviceType` == deviceType &&
          `deviceId` == deviceId =>
    }
  }

  "registerDevice" should "register device to user" in {
    registerDevice(ownerUserId, hubDevice, secureCode).await shouldBe ()
    CheckDBContent.isExistOwnership
  }

}