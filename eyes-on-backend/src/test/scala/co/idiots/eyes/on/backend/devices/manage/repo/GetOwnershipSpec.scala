package co.idiots.eyes.on.backend.devices.manage.repo

import co.idiots.eyes.on.message.{DeviceUUID, Ownership}

class GetOwnershipSpec extends PostgresTestSupportKit with GetOwnership {

  val userId = 1
  val ownerType = "admin"
  val deviceType = "seed"
  val deviceId = 1
  val deviceUUID = DeviceUUID(deviceType, deviceId)

  dbInsertQToBeforeEach.append(
    s"""
      |INSERT INTO device (device_type, device_id, secure_code) VALUES ('$deviceType', $deviceId, 'code');
      |INSERT INTO ownership (user_id, device_type, device_id, owner_type) VALUES ($userId, '${deviceUUID.`type`}', ${deviceUUID.id}, '$ownerType')
    """.stripMargin)

  "getOwnership" should "return specific device's ownership information" in {
    getOwnership(deviceUUID).await shouldBe Ownership(userId, deviceUUID, ownerType)
  }
}