package co.idiots.eyes.on.backend.devices.storage.repo

import co.idiots.eyes.on.backend.devices.AwaitableFuture
import co.idiots.eyes.on.message.{NotFound, Payload}
import com.mongodb.DBObject
import com.mongodb.casbah.Imports.MongoDBObject
import org.joda.time.{DateTime, DateTimeZone}
import org.scalatest.{BeforeAndAfterEach, FlatSpecLike, Matchers}

trait MongoTestSupportKit extends FlatSpecLike with Matchers with AwaitableFuture with BeforeAndAfterEach {

  override def beforeEach(): Unit = {
    collection.remove(MongoDBObject.newBuilder.result)
  }

  protected def insertObjectToMongoDb(timestamp: DateTime, payload: Payload) = {
    val objectForInsert = MongoDBObject(
      "timestamp" -> timestamp.toDate,
      "device_type" -> payload.deviceUUID.`type`,
      "device_id" -> payload.deviceUUID.id,
      "sensor_value" ->
        MongoDBObject(
          "soil" -> payload.sensorValue.soil,
          "temp" -> payload.sensorValue.temp,
          "humid" -> payload.sensorValue.humid),
      "detection" ->
        MongoDBObject(
          "water_detection" -> payload.detection.waterDetection,
          "refresh_detection" -> payload.detection.refreshDetection))
    collection.insert(objectForInsert)
  }

  protected def getOneObjectFromMongoDb(keyObject: DBObject) = {
    collection.findOne(keyObject) match {
      case Some(objectFromMongo) => objectFromMongo
      case None => throw NotFound("not found object from mongo db")
    }
  }

}
