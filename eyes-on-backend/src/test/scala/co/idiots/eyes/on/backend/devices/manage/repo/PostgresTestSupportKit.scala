package co.idiots.eyes.on.backend.devices.manage.repo

import co.idiots.eyes.on.backend.devices.AwaitableFuture
import org.scalatest.{FlatSpec, Matchers}

trait PostgresTestSupportKit
  extends FlatSpec
    with PostgresTestKit
    with Matchers
    with AwaitableFuture