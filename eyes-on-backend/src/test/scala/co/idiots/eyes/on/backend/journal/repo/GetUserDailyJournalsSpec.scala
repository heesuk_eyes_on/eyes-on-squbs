package co.idiots.eyes.on.backend.journal.repo

import co.idiots.eyes.on.message.Journal
import org.joda.time.{DateTime, DateTimeZone}

class GetUserDailyJournalsSpec extends PostgresTestSupportKit with GetUserDailyJournals {

  val yesterDayJournalId = 1
  val todayJournalId = 2
  val userId = 1
  val defaultSubject = "제목을 입력하세요."
  val defaultContent = "내용을 입력하세요."
  val weatherStatus = "Clear"
  val dailyMaxTemp = 1
  val dailyMinTemp = 1
  val dailyHumid = 1
  val koreaDateTime = new DateTime().withZone(DateTimeZone.forID("Asia/Seoul")).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0)
  val createdAt = new DateTime("2017-11-27T21:50:13.305+09:00")
  val updatedAt = new DateTime("2017-11-27T21:50:13.305+09:00")
  val fromDay = new DateTime(koreaDateTime).withZone(DateTimeZone.UTC).minusDays(1)
  val toDay = new DateTime(koreaDateTime).withZone(DateTimeZone.UTC).plusDays(1)

  val yesterDayJournal = Journal(
    Some(yesterDayJournalId),
    userId,
    defaultSubject,
    defaultContent,
    weatherStatus,
    dailyMaxTemp,
    dailyMinTemp,
    dailyHumid,
    koreaDateTime.minusDays(1),
    createdAt,
    updatedAt
  )
  val todayJournal = Journal(
    Some(todayJournalId),
    userId,
    defaultSubject,
    defaultContent,
    weatherStatus,
    dailyMaxTemp,
    dailyMinTemp,
    dailyHumid,
    koreaDateTime,
    createdAt,
    updatedAt
  )

  val desc = true

  dbInsertQToBeforeEach.append(
    s"""
       |INSERT INTO journal (journal_id, user_id, subject, content, weather_status, daily_max_temp, daily_min_temp, daily_humid, datetime, created_at, updated_at)
       |VALUES ($yesterDayJournalId, $userId, '$defaultSubject', '$defaultContent', '$weatherStatus', $dailyMaxTemp, $dailyMinTemp, $dailyHumid, '${koreaDateTime.minusDays(1)}', '$createdAt', '$updatedAt');
       |
       |INSERT INTO journal (journal_id, user_id, subject, content, weather_status, daily_max_temp, daily_min_temp, daily_humid, datetime, created_at, updated_at)
       |VALUES ($todayJournalId, $userId, '$defaultSubject', '$defaultContent', '$weatherStatus', $dailyMaxTemp, $dailyMinTemp, $dailyHumid, '$koreaDateTime', '$createdAt', '$updatedAt');
  """.stripMargin)


  "getUserDailyJournals" should "return specific user`s daily journals" in {
    getUserDailyJournals(userId, fromDay, toDay).await should matchPattern {
      case Vector(todayJournal, yesterdayJournal) =>
    }
  }
}
