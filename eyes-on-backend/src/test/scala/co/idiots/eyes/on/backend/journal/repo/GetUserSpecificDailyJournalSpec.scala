package co.idiots.eyes.on.backend.journal.repo

import co.idiots.eyes.on.message.Journal
import org.joda.time.{DateTime, DateTimeZone}

class GetUserSpecificDailyJournalSpec extends PostgresTestSupportKit with GetUserSpecificDailyJournal {

  val journalId = 1
  val userId = 1
  val defaultSubject = "제목을 입력하세요."
  val defaultContent = "내용을 입력하세요."
  val weatherStatus = "Clear"
  val dailyMaxTemp = 1
  val dailyMinTemp = 1
  val dailyHumid = 1
  val koreaDateTime = new DateTime().withZone(DateTimeZone.forID("Asia/Seoul")).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0)
  val createdAt = new DateTime("2017-11-27T21:50:13.305+09:00")
  val updatedAt = new DateTime("2017-11-27T21:50:13.305+09:00")


  val specificJournal = Journal(
    Some(journalId),
    userId,
    defaultSubject,
    defaultContent,
    weatherStatus,
    dailyMaxTemp,
    dailyMinTemp,
    dailyHumid,
    koreaDateTime,
    createdAt,
    updatedAt
  )

  dbInsertQToBeforeEach.append(
    s"""
      |INSERT INTO journal (journal_id, user_id, subject, content, weather_status, daily_max_temp, daily_min_temp, daily_humid, datetime, created_at, updated_at)
      |VALUES ($journalId, $userId, '$defaultSubject', '$defaultContent', '$weatherStatus', $dailyMaxTemp, $dailyMinTemp, $dailyHumid, '$koreaDateTime', '$createdAt', '$updatedAt')
    """.stripMargin)

  "getUserSpecificDailyJournal" should "return specific daily journal" in {
    getUserSpecificDailyJournal(journalId).await should matchPattern {
      case Journal(Some(jId), `userId`, subject, content, `weatherStatus`, _, _, _, datetime, _, _)
        if jId == journalId && `userId` == userId && subject == defaultSubject && content == defaultContent && datetime == koreaDateTime =>
    }
  }
}