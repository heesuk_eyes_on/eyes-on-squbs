package co.idiots.eyes.on.backend.detection.repo

import co.idiots.eyes.on.message.{DeviceUUID, IsDetected}
import org.joda.time.{DateTime, DateTimeZone}

class UpsertDetectionSpec extends PostgresTestSupportKit with UpsertDetection {

  import dbContext._

  val userId = 1
  val deviceType = "seed"
  val deviceId = 1
  val deviceUUID = DeviceUUID(deviceType, deviceId)
  val datetime: DateTime = new DateTime("2017-11-25T00:00:00.000Z").withZone(DateTimeZone.UTC)
  val koreaDateTime: DateTime = datetime.withZone(DateTimeZone.forID("Asia/Seoul")).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0)
  val DetectedWater = true
  val UndetectedRefresh = false
  val detected = IsDetected(DetectedWater, UndetectedRefresh)
  val waterCount = 1
  val refreshCount= 0
  val detection = SQLEntities.Detection(userId, deviceType, deviceId, new DateTime(koreaDateTime).withZone(DateTimeZone.UTC), waterCount, refreshCount)

  private trait CheckDbContent {
    lazy val mockIsExistNewDetectedEvent: Unit = dbContext.run(
      quote(
        query[SQLEntities.Detection]
          .filter(x => x.userId == lift(userId) &&
            x.deviceType == lift(deviceType) &&
            x.deviceId == lift(deviceId) &&
            x.datetime == lift(datetime)))
      ).headOption shouldBe Some(detection)
    lazy val insertToAleadyExistDetectedEvent: Unit = dbContext.run(
      quote(
        query[SQLEntities.Detection]
          .insert(lift(SQLEntities.Detection(userId, deviceType, deviceId, koreaDateTime, waterCount, refreshCount)))
      ))
    lazy val mockCheckWaterCountIncrement: Unit = dbContext.run(
      quote(
        query[SQLEntities.Detection]
          .filter(x => x.userId == lift(userId) &&
            x.deviceType == lift(deviceType) &&
            x.deviceId == lift(deviceId) &&
            x.datetime == lift(datetime)))
    ).headOption shouldBe Some(detection.copy(waterCount = 2))
  }

  "upsertDetection" should "generates the number of events detected on a specific day for the first event" in new CheckDbContent {
    upsertDetection(userId, deviceUUID, datetime, detected).await shouldBe ()
    mockIsExistNewDetectedEvent
  }

  it should "increment detection count if aleady exist detected event" in new CheckDbContent {
    insertToAleadyExistDetectedEvent
    upsertDetection(userId, deviceUUID, datetime, detected).await shouldBe ()
    mockCheckWaterCountIncrement
  }
}