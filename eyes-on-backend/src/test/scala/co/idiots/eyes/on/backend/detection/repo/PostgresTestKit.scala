package co.idiots.eyes.on.backend.detection.repo

import org.scalatest.{BeforeAndAfterEach, Suite}

import scala.concurrent.ExecutionContext

trait PostgresTestKit extends BeforeAndAfterEach { self: Suite =>

  import dbContext._

  implicit val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global

  val dbInsertQToBeforeEach = new StringBuilder

  val baseDatabase: String =
    """
      |DROP TABLE IF EXISTS public.detection CASCADE;
      |
      |CREATE TABLE detection
      |(
      |  user_id integer NOT NULL,
      |  device_type character varying(255) NOT NULL,
      |  device_id integer NOT NULL,
      |  water_count integer NOT NULL,
      |  refresh_count integer NOT NULL,
      |  datetime date DEFAULT now() NOT NULL,
      |  CONSTRAINT user_id_device_type_device_id_datetime PRIMARY KEY (user_id, device_type, device_id, datetime)
      |)
      |WITH (
      |  OIDS=FALSE
      |);
      |ALTER TABLE detection
      |  OWNER TO root;
    """.stripMargin

  dbInsertQToBeforeEach.append(baseDatabase)

  override def beforeEach(): Unit = {
    super.beforeEach()

    executeAction(
      dbInsertQToBeforeEach.toString
    )
  }

  override def afterEach(): Unit = {
    dbContext.executeAction(
      """
        |DROP TABLE IF EXISTS public.detection CASCADE;
      """.stripMargin)
    super.afterEach()
  }

}

