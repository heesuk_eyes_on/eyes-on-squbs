package co.idiots.eyes.on.backend.devices

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

trait AwaitableFuture {

  implicit class AwaitableFuture[T](future: Future[T]) {
    def await: T = Await.result(future, 3.seconds)
  }

}

