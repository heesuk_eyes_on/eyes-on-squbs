package co.idiots.eyes.on.backend.devices.storage.repo

import co.idiots.eyes.on.message.{IsDetected, DeviceUUID, Payload, SensorValue}
import org.joda.time.DateTime
import play.api.libs.json.JsArray

class GetDeviceEventSpec extends MongoTestSupportKit with GetDeviceEvent {


  val from = new DateTime("2017-11-21T00:00:00.000Z")
  val timestamp = new DateTime("2017-11-25T00:00:00.000Z")
  val to = new DateTime()
  val limit = 1
  val desc = true

  val deviceType = "seed"
  val deviceId = 1
  val deviceUUID: DeviceUUID = DeviceUUID(deviceType, deviceId)
  val soil = 30
  val temp = 30
  val humid = 30
  val sensorValue = SensorValue(soil, temp, humid)
  val detection = IsDetected(false, false)
  val payload = Payload(deviceUUID, sensorValue, detection)

  "getDeviceEvent" should "retrieve device events" in {
    insertObjectToMongoDb(timestamp, payload)
    getDeviceEvent(deviceUUID, from, to, limit, desc).await should matchPattern {
      case JsArray(items)
        if (items.head \ "device_type").validate[String].get == deviceType &&
          (items.head \ "device_id").validate[Int].get == deviceId &&
          (items.head \ "sensor_value" \ "soil").validate[Double].get == soil &&
          (items.head \ "sensor_value" \ "temp").validate[Double].get == temp &&
          (items.head \ "sensor_value" \ "humid").validate[Double].get == humid &&
          (items.head \ "detection" \ "water_detection").validate[Boolean].get.equals(false) &&
          (items.head \ "detection" \ "refresh_detection").validate[Boolean].get.equals(false) =>
    }
  }
}
