package co.idiots.eyes.on.backend.journal.repo

import co.idiots.eyes.on.backend.devices.AwaitableFuture
import org.scalatest.{FlatSpecLike, Matchers}

trait PostgresTestSupportKit
  extends FlatSpecLike
    with PostgresTestKit
    with Matchers
    with AwaitableFuture
