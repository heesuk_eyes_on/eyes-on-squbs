package co.idiots.eyes.on.backend.devices.storage.repo

import co.idiots.eyes.on.message.{IsDetected, DeviceUUID, Payload, SensorValue}
import com.mongodb.casbah.Imports.DBObject

class CreateDeviceEventSpec extends MongoTestSupportKit {

  val deviceType = "seed"
  val deviceId = 1
  val deviceUUID: DeviceUUID = DeviceUUID(deviceType, deviceId)
  val soilValue = 30.0
  val tempValue = 30.0
  val humidValue = 30.0
  val sensorValue = SensorValue(soilValue, tempValue, humidValue)
  val detection = IsDetected(false, false)
  val payload = Payload(deviceUUID, sensorValue, detection)

  val keyObject: DBObject = DBObject("device_type" -> deviceUUID.`type`, "device_id" -> deviceUUID.id)

  "createDeviceEvent" should "create device envet" in {
    createDeviceEvent(deviceUUID, payload).await shouldBe ()
    val objectFromMongoDb = getOneObjectFromMongoDb(keyObject)
    objectFromMongoDb.get("device_type") shouldBe "seed"
    objectFromMongoDb.get("device_id") shouldBe 1
    objectFromMongoDb.get("sensor_value") shouldBe DBObject("soil" -> soilValue, "temp" -> tempValue, "humid" -> humidValue)
    objectFromMongoDb.get("detection") shouldBe DBObject("water_detection" -> false, "refresh_detection" -> false)
  }

}
