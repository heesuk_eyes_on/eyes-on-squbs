package co.idiots.eyes.on.backend.devices.manage.repo

import org.scalatest.{BeforeAndAfterEach, Suite}

import scala.concurrent.ExecutionContext

trait PostgresTestKit extends BeforeAndAfterEach { self: Suite =>

  import dbContext._

  implicit val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global

  val dbInsertQToBeforeEach = new StringBuilder

  val baseDatabase: String =
    """
      |DROP TABLE IF EXISTS public.device CASCADE;
      |DROP TABLE IF EXISTS public.ownership CASCADE;
      |DROP TABLE IF EXISTS public.hub_group_device CASCADE;
      |
      |CREATE TABLE device
      |(
      |  device_type character varying(255) NOT NULL,
      |  device_id integer NOT NULL,
      |  device_name character varying(255),
      |  secure_code character varying(255) NOT NULL,
      |  created_at TIMESTAMP DEFAULT now() NOT NULL,
      |  updated_at TIMESTAMP DEFAULT now() NOT NULL,
      |  CONSTRAINT type_id_pk PRIMARY KEY (device_type, device_id)
      |)
      |WITH (
      |  OIDS=FALSE
      |);
      |ALTER TABLE device
      |  OWNER TO root;
      |
      |CREATE TABLE ownership
      |(
      |  user_id integer NOT NULL,
      |  device_type character varying(255) NOT NULL,
      |  device_id integer NOT NULL,
      |  owner_type character varying(255) NOT NULL,
      |  created_at timestamp DEFAULT now() NOT NULL,
      |  updated_at timestamp DEFAULT now() NOT NULL,
      |  CONSTRAINT ownership_user_id_device_type_device_id_user_id_pk PRIMARY KEY (user_id, device_type, device_id),
      |  CONSTRAINT ownership_device_device_type_device_id_fk FOREIGN KEY (device_type, device_id)
      |  REFERENCES device (device_type, device_id)
      |  ON UPDATE CASCADE ON DELETE CASCADE
      |)
      |WITH (
      |  OIDS=FALSE
      |);
      |ALTER TABLE ownership
      |  OWNER TO root;
      |
      |CREATE TABLE hub_group_device
      |(
      |  seed_id integer not null,
      |  hub_id integer not null,
      |  created_at TIMESTAMP DEFAULT now() NOT NULL,
      |  CONSTRAINT seed_hub_id PRIMARY KEY (seed_id, hub_id)
      |)
      |WITH (
      |  OIDS=FALSE
      |);
      |ALTER TABLE hub_group_device
      |  OWNER TO root;
    """.stripMargin

  dbInsertQToBeforeEach.append(baseDatabase)

  override def beforeEach(): Unit = {
    super.beforeEach()

    executeAction(
      dbInsertQToBeforeEach.toString
    )
  }

  override def afterEach(): Unit = {
    dbContext.executeAction(
      """
        |DROP TABLE IF EXISTS public.device CASCADE;
        |DROP TABLE IF EXISTS public.ownership CASCADE;
        |DROP TABLE IF EXISTS public.hub_group_device CASCADE;
      """.stripMargin)
    super.afterEach()
  }

}
