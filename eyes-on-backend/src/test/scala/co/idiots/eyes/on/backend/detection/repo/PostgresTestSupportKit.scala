package co.idiots.eyes.on.backend.detection.repo

import co.idiots.eyes.on.backend.devices.AwaitableFuture
import org.scalatest.{FlatSpec, Matchers}

trait PostgresTestSupportKit
  extends FlatSpec
    with PostgresTestKit
    with Matchers
    with AwaitableFuture
