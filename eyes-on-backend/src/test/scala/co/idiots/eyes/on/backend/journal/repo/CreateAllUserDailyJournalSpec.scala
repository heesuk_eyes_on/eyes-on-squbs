package co.idiots.eyes.on.backend.journal.repo
import akka.util.Timeout
import co.idiots.eyes.on.backend.devices.AwaitableFuture
import co.idiots.eyes.on.message.{User, Users, Weather}
import co.idiots.eyes.on.util.TestKit

import scala.concurrent.duration._

class CreateAllUserDailyJournalSpec extends TestKit(false)
  with CreateAllUserDailyJournal
  with AwaitableFuture
  with PostgresTestSupportKit {

  import dbContext._
  override def akkaTimeout: Timeout = Timeout(3.seconds)

  val dailyMaxTemp = 1
  val dailyMinTemp = 1
  val dailyHumid = 1
  val datetimeLong = 1511751600
  val userId = 1
  val journalId = 1
  val defaultSubject = "제목을 입력하세요."
  val defaultContent = "내용을 입력하세요."

  private trait MockObjects {
    lazy val mockCurrentDailyWeatherRequest: Unit = addStub {
      case Weather.GetCurrentDailyWeatherRequest() =>
        Weather(dailyMaxTemp, dailyMinTemp, dailyHumid, datetimeLong, "Clear")
    }
    lazy val mockGetAllUsersInfo: Unit = addStub {
      case Users.GetRequest() =>
        Users(
          Vector(
            User(userId, "01012345678")))
    }
    lazy val isExistUserDailyJournal: Unit  =
      dbContext.run {
        quote {
          query[SQLEntities.Journal]
        }
      }.head should matchPattern {
        case SQLEntities.Journal(`journalId`, `userId`, subject, content, weatherStatus, `dailyMaxTemp`, `dailyMinTemp`, `dailyHumid`, datetime, _, _)
          if `journalId` == journalId &&
            `userId` == userId &&
            subject == defaultSubject &&
            content == defaultContent &&
            weatherStatus == "Clear" =>
      }
  }

  "createAllUserDailyJournal" should "create all user daily journal" in new MockObjects {
    mockCurrentDailyWeatherRequest
    mockGetAllUsersInfo
    createAllUserDailyJournal.await shouldBe ()
    isExistUserDailyJournal
  }
}
