package co.idiots.eyes.on.backend.devices.manage.repo

import co.idiots.eyes.on.message.DeviceUUID

class RegisterEndDeviceSpec extends PostgresTestSupportKit with RegisterEndDevice {

  val ownerUserId = 1

  val deviceTypeForHub = "hub"
  val deviceIdForHub = 0
  val deviceHubUUID = DeviceUUID(deviceTypeForHub, deviceIdForHub)

  val deviceTypeForSeed = "seed"
  val deviceIdForSeed = 1
  val deviceSeedUUID = DeviceUUID(deviceTypeForHub, deviceIdForSeed)

  val secureCode = "foo"

  "registerEndDevice" should "register end device to hub device" in {
    registerEndDevice(ownerUserId, deviceHubUUID, deviceSeedUUID, secureCode).await shouldBe ()
  }
}