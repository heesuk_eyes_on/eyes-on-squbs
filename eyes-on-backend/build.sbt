import Versions._

val quillVersion = "1.3.0"

libraryDependencies ++= Seq(
  "com.typesafe.play" %% "play-json" % "2.5.9",
  "org.fusesource.mqtt-client" % "mqtt-client" % "1.12",
  "org.squbs" %% "squbs-testkit" % squbs % "test",
  "com.typesafe.akka" %% "akka-actor" % akka,
  "org.mongodb" %% "casbah" % "3.1.1",
  "org.postgresql" % "postgresql" % "9.4.1208",
  "io.getquill" %% "quill-jdbc" % quillVersion,
  "io.getquill" %% "quill-cassandra" % "1.0.0",
  "org.reactivemongo" %% "play2-reactivemongo" % "0.11.14",
  "com.github.t3hnar" %% "scala-bcrypt" % "3.0",
  "com.typesafe.akka" %% "akka-http-spray-json" % "10.0.10",
  "com.enragedginger" %% "akka-quartz-scheduler" % "1.6.0-akka-2.4.x",
  "io.spray" %% "spray-client" % spray)
//TODO : 왜 각 멀티 모듈이 하나의 SBT에서 접근이 안되는지? 그러니까 중복해서 라이브러리를 각 프로젝트마다 받는 문제가 있다.