object Versions {
  val squbs = "0.8.1"
  val akka = "2.4.11"
  val spray = "1.3.3"
}