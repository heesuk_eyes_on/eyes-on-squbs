import Versions._

libraryDependencies ++= Seq(
  "com.auth0" % "java-jwt" % "2.2.1",
  "org.squbs" %% "squbs-testkit" % squbs,
  "org.squbs" %% "squbs-actorregistry" % squbs,
  "io.spray" %% "spray-testkit" % spray,
  "org.scalatest" %% "scalatest" % "2.2.1"
)