package co.idiots.eyes.on.util

import java.util.concurrent.atomic.AtomicInteger

import akka.actor.ActorRef
import akka.testkit.TestActor.AutoPilot
import akka.testkit.{TestActor, TestProbe}
import co.idiots.eyes.on.message._
import com.typesafe.config.{Config, ConfigFactory}
import org.slf4j.LoggerFactory
import org.squbs.testkit.CustomRouteTestKit

import scala.util.{Failure, Success, Try}

object TestKit {
  // To prevent several minutes hanging when initializing squbs unicomplex booting on test environment
  System.setProperty("startup.timeout", "3000")

  private val log = LoggerFactory.getLogger(classOf[TestKit])

  private def childSpecClass = Class.forName((new Exception).getStackTrace.find(_.getClassName.matches(".*Spec$")).get.getClassName)

  private def getMeta(name: String): Option[String] = {
    Try {
      childSpecClass.getClassLoader.getResource(name + "META-INF/squbs-meta.conf").getPath
    } match {
      case Success(s) =>
        Some(s)
      case Failure(ex: NullPointerException) =>
        log.warn(s"squbs-meta in $name does not exist")
        None
      case Failure(ex) =>
        throw ex
    }
  }

  val counter = new AtomicInteger(0)

  def actorSystemNameFrom(className: String): String =
    className
      .replace('.', '-')
      .replace('_', '-')
      .filter(_ != '$')

  def defaultActorSystemName: String = {
    s"${actorSystemNameFrom(childSpecClass.getName)}-${counter.getAndIncrement()}"
  }
}

abstract class TestKit(config: Config, withRootMeta: Boolean, stubActorName: String)
  extends CustomRouteTestKit(
    config,
    List(
      if (withRootMeta) TestKit.getMeta("") else None,
      TestKit.getMeta(s"${TestKit.childSpecClass.getSimpleName}/"),
      TestKit.getMeta("ActorRegistryCube/")).flatten,
    false) {

  @volatile private var stubPf: PartialFunction[Any, Response] = _
  protected val probe = TestProbe()

  system.actorSelection(s"/user/${TestKit.childSpecClass.getSimpleName}/$stubActorName") ! probe.ref

  def this(withRootMeta: Boolean, stubActorName: String = "ForwardActor") = this(ConfigFactory.parseString(
    s"""
       |squbs {
       |  actorsystem-name = ${TestKit.defaultActorSystemName}
       |}
       |default-listener.bind-port = 0
    """.stripMargin), withRootMeta, stubActorName)

  def this(config: Config, withRootMeta: Boolean) = this(config, withRootMeta, "ForwardActor")

  protected def stub(pf: PartialFunction[Any, Response]): Unit = {
    stubPf = pf
    probe.setAutoPilot(new TestActor.AutoPilot {
      override def run(sender: ActorRef, msg: Any): AutoPilot = {
        sender ! stubPf(msg)
        TestActor.KeepRunning
      }
    })
  }

  protected def addStub(pf: PartialFunction[Any, Response]): Unit = {
    stubPf = pf.orElse(stubPf)
    probe.setAutoPilot(new TestActor.AutoPilot {
      override def run(sender: ActorRef, msg: Any): AutoPilot = {
        sender ! stubPf(msg)
        TestActor.KeepRunning
      }
    })
  }

  override def afterAll(): Unit = {
    super.afterAll()
    TestKit.log.info("Wait until ActorSystem fully shutdown")
    akka.testkit.TestKit.shutdownActorSystem(system)
  }
}
