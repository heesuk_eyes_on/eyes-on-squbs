package co.idiots.eyes.on.util.handler


import akka.actor.ActorRefFactory
import com.typesafe.config.Config
import org.squbs.pipeline._
import spray.http.ContentTypes._
import spray.http.{HttpEntity, HttpResponse}

import scala.concurrent.Future

class JsonEnforceHandler extends Handler with HandlerFactory {

  override def process(reqCtx: RequestContext)(implicit context: ActorRefFactory): Future[RequestContext] =
    reqCtx match {
      case RequestContext(_, false, NormalResponse(resp@ HttpResponse(_, entity@ HttpEntity.NonEmpty(`application/json`, data), _, _)), _) =>
        val stringify = data.asString
        if (stringify.startsWith("{"))
          Future.successful(
            reqCtx.copy(response = NormalResponse(
              resp.copy(entity = HttpEntity(`application/json`, stringify)))))
        else
          Future.successful(
            reqCtx.copy(response = NormalResponse(
              resp.copy(entity = HttpEntity(`application/json`, s"""{"message":$stringify}""")))))
      case RequestContext(_, false, NormalResponse(resp@ HttpResponse(_, entity@ HttpEntity.NonEmpty(`text/plain`, data), _, _)), _) =>
        Future.successful(
          reqCtx.copy(response = NormalResponse(
            resp.copy(entity = HttpEntity(`application/json`, s"""{"message":"${data.asString}"}""")))))
      case RequestContext(_, false, NormalResponse(resp@ HttpResponse(_, entity@ HttpEntity.NonEmpty(`text/plain(UTF-8)`, data), _, _)), _) =>
        Future.successful(
          reqCtx.copy(response = NormalResponse(
            resp.copy(entity = HttpEntity(`application/json`, s"""{"message":"${data.asString}"}""")))))
      case _ =>
        Future.successful(reqCtx)
    }

  override def create(config: Option[Config])(implicit actorRefFactory: ActorRefFactory): Option[Handler] = Some(this)
}
