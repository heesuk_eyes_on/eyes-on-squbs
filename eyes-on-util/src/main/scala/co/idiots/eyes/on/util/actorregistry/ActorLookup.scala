package co.idiots.eyes.on.util.actorregistry


import akka.actor.{ActorRef, ActorRefFactory}
import akka.util.Timeout
import co.idiots.eyes.on.message.{ErrorResponse, InternalError, Response}
import org.joda.time.DateTime
import org.slf4j.LoggerFactory

import scala.concurrent.{ExecutionContext, Future}
import scala.reflect.ClassTag
import scala.util.{Failure, Success}

object ActorLookup {
  def apply[T <: Response: ClassTag]: ActorLookup[T] = new ActorLookup[T]
  def apply[T <: Response: ClassTag](name: String): ActorLookup[T] = new ActorLookup[T](Some(name))
  def forward(target: String)
             (req: Any, sender: ActorRef)
             (implicit ex: ExecutionContext, actorRefFactory: ActorRefFactory, timeout: Timeout): Unit = {
    (org.squbs.actorregistry.ActorLookup[Any](target) ? req).onComplete {
      case Success(msg) => sender ! msg
      case Failure(ex) => sender ! ex
    }
  }
}

private[actorregistry] class ActorLookup[T <: Response: ClassTag](name: Option[String] = None) {

  private val log = LoggerFactory.getLogger("co.bitfinder.awair.squbs.util.actorregistry.ActorLookup")

  def ?(message: Any)(implicit timeout: Timeout, refFactory: ActorRefFactory, ec: ExecutionContext): Future[T] = {
    val startTime = new DateTime()
    val f = name match {
      case None => org.squbs.actorregistry.ActorLookup ? message
      case Some(s) => org.squbs.actorregistry.ActorLookup[Any](s) ? message
    }
    def logLatency(isSuccess: Boolean) = {
      val latency = new DateTime().getMillis - startTime.getMillis
      log.info(s"${message.getClass.getName} ${if (isSuccess) "success" else "failure"} $latency")
    }
    f.flatMap {
      case t: T =>
        logLatency(true)
        Future.successful(t)
      case er: ErrorResponse =>
        logLatency(true)
        Future.failed(er)
      case any =>
        logLatency(false)
        Future.failed(InternalError(s"""Unexpected response "$any" of request "$message""""))
    }
  }
}
