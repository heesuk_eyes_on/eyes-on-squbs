package co.idiots.eyes.on.util.json

import play.api.libs.json.{JsDefined, JsObject, JsValue, Json}

trait JsObjectExtension {
  val js: JsObject

  def replace(keys: (String, String)*): JsObject = {
    keys.foldLeft(js) { (acc, key) =>
      acc \ key._1 match {
        case JsDefined(value) => acc + (key._2 -> value) - key._1
        case _ => acc
      }
    }
  }

  def expose(keys: String*): JsObject = {
    keys.foldLeft(Json.obj()) { (acc, key) =>
      js \ key match {
        case JsDefined(value) => acc + (key -> value)
        case _ => acc
      }
    }
  }

  def exclude(keys: String*): JsObject = {
    keys.foldLeft(js) { (acc, key) =>
      acc - key
    }
  }

  def duplicate(keys: (String, String)*): JsObject = {
    keys.foldLeft(js) { (acc, key) =>
      js \ key._1 match {
        case JsDefined(value) => acc + (key._2 -> value)
        case _ => acc
      }
    }
  }
}

trait ToJsObjectExtension {
  implicit def toJsObjectExtension(j: JsValue): JsObjectExtension = new JsObjectExtension {
    val js = j.as[JsObject]
  }
}
