package co.idiots.eyes.on.util


import akka.actor.{Actor, ActorRef}

class ForwardActor extends Actor {
  var actor: ActorRef = _

  override def receive: Receive = {
    case a: ActorRef => actor = a
    case msg => actor forward msg
  }
}
