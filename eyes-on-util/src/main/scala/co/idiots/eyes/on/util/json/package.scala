package co.idiots.eyes.on.util

import org.joda.time.{DateTime, DateTimeZone}
import play.api.libs.json._

import scala.util.Try

package object json extends ToJsObjectExtension {
  val dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
  implicit val jodaDateTimeReads = Reads.jodaDateReads(dateFormat)
  implicit val jodaDateTimeWrites = Writes[DateTime](dt => JsString(dt.withZone(DateTimeZone.UTC).toString))
  implicit val jodaDateTimeZoneReads = Reads[DateTimeZone] { js =>
    js.validate[String].map(str => Try(DateTimeZone.forID(str)).toOption) flatMap {
      case Some(tz) => JsSuccess(tz)
      case None => JsError("Invalid timezone format")
    }
  }
  implicit val jodaDateTimeZoneWrites = Writes[DateTimeZone] { tz => JsString(tz.toString) }
}
