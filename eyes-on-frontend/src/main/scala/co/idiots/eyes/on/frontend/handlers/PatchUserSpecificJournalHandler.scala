package co.idiots.eyes.on.frontend.handlers

import co.idiots.eyes.on.frontend.forms.PatchUserSpecificJournalForm
import play.api.libs.json.Json
import spray.routing.Route

trait PatchUserSpecificJournalHandler {

  def patchUserSpecificJournal(form: PatchUserSpecificJournalForm): Route = { ctx =>
    ctx.complete(Json.parse(
      s"""
         |{
         |  "subject": "${form.subject}",
         |  "content": "${form.content}"
         |}
          """.stripMargin))
  }
}
