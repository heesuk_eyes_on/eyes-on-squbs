package co.idiots.eyes.on.frontend.handlers

import akka.actor.ActorSystem
import akka.util.Timeout
import co.idiots.eyes.on.message.{Device, DeviceUUID, Empty}
import co.idiots.eyes.on.util.actorregistry.ActorLookup
import com.auth0.jwt.JWTSigner
import com.typesafe.config.ConfigFactory
import play.api.libs.json.Json
import spray.routing.Route

import scala.collection.JavaConversions.mapAsJavaMap
import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

trait PostHubDeviceOwnershipHandler {

  implicit def system: ActorSystem
  implicit def executionContext: ExecutionContext
  implicit def akkaTimeout: Timeout

  private val deviceJwtSecret: String = ConfigFactory.load.getString("device.jwt.secret")
  def getDeviceToken(`type`: String, id: Int): String = {
    val claims = mapAsJavaMap(Map("device_type" -> `type`, "device_id" -> id.toString))
      .asInstanceOf[java.util.Map[String, AnyRef]]
    new JWTSigner(deviceJwtSecret).sign(claims)
  }

  def postHubDeviceOwnership(ownerUserId: Int, hubDevice: DeviceUUID, secureCode: String): Route = { ctx =>
    ActorLookup[Empty] ? Device.RegisterRequest(ownerUserId, hubDevice, secureCode) onComplete {
      case Success(_) => ctx.complete(Json.obj("device_token" -> getDeviceToken(hubDevice.`type`, hubDevice.id)))
      case Failure(ex) => ctx.complete(500, ex.getMessage)
    }
  }
}
