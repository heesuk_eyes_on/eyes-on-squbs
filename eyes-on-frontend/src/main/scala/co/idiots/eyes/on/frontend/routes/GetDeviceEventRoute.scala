package co.idiots.eyes.on.frontend.routes

import co.idiots.eyes.on.frontend.authdirectives.AuthUser
import co.idiots.eyes.on.frontend.handlers.GetDeviceEventHandler
import co.idiots.eyes.on.message.DeviceUUID
import org.joda.time.DateTime
import spray.routing.{Directives, Route}

trait GetDeviceEventRoute extends Directives with AuthUser with GetDeviceEventHandler {
  //TODO: parameter bug `from`, `to` nothing type
  def getDeviceEventRoute: Route = {
    (get & path("users" / "self" / "devices" / Segment / IntNumber / "events" / "raw-data")) { (deviceType, deviceId) =>
      parameters('from ? "1970-01-01T00:00:00.000Z", 'to ? s"${new DateTime().toString}", 'limit ? 1000, 'desc.as[Boolean] ? true) { (from, to, limit, desc) =>
        authInternalUser { userId =>
          getDeviceEvent(DeviceUUID(deviceType, deviceId), from, to, 10000, true)
        }
      }
    }
  }
}