package co.idiots.eyes.on.frontend.forms

import co.idiots.eyes.on.util.json.JsonSnakeCase
import play.api.libs.json.Reads

case class PostDeviceOwnershipForm(deviceType: String, deviceId: Int, secureCode: String)

object PostDeviceOwnershipForm {
  implicit val postHubDeviceOwnershipFormReads: Reads[PostDeviceOwnershipForm] = JsonSnakeCase.reads[PostDeviceOwnershipForm]
}
