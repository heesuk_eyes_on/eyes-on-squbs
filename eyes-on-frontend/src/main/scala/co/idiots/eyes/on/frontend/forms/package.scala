package co.idiots.eyes.on.frontend

import spray.httpx.PlayJsonSupport

package object forms extends PlayJsonCodecs with PlayJsonSupport
