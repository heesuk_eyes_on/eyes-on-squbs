package co.idiots.eyes.on.frontend.routes

import co.idiots.eyes.on.frontend.authdirectives.AuthUser
import co.idiots.eyes.on.frontend.handlers.GetUserJournalsHandler
import org.joda.time.{DateTime, DateTimeZone}
import spray.httpx.PlayJsonSupport
import spray.routing.{Directives, Route}

trait GetUserJournalsRoute extends Directives with AuthUser with PlayJsonSupport with GetUserJournalsHandler {

  def getUserJournalsRoute: Route = {
    (get & path("users" / "self" / "journals")) {
      parameter('from ? "1970-01-01T00:00:00.000Z") { (from) =>
        parameter('to ? new DateTime(DateTimeZone.UTC).toString) { to =>
          authInternalUser { userId =>
            getUserJournals(userId, from, to)
          }
        }
      }
    }
  }
}