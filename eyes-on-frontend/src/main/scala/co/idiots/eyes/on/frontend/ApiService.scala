package co.idiots.eyes.on.frontend
/**
  * Created by heesuk on 2017. 5. 1..
  */
import akka.actor.ActorSystem
import akka.util.Timeout
import co.idiots.eyes.on.frontend.routes.Routes
import co.idiots.eyes.on.util.spray.CorsSupport
import com.typesafe.config.ConfigFactory
import org.squbs.unicomplex.RouteDefinition
import spray.httpx.PlayJsonSupport._
import spray.routing.Route

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

/**
  * Created by heesuk on 2017. 5. 26
  */
class ApiService extends RouteDefinition
  with CorsSupport
  with PlayJsonCodecs {

  implicit val akkaTimeout: Timeout = Timeout(3.seconds)
  implicit val system: ActorSystem = context.system
  implicit val executionContext: ExecutionContext = context.dispatcher

  val routeContainer: Route = new Routes().routeContainer

  override def route: Route = {
    (path("") & get) { ctx =>
      ctx.complete("")
    } ~
    pathPrefix("v1") {
      routeContainer
    }
  }
}