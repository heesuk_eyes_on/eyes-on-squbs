package co.idiots.eyes.on.frontend.forms

import co.idiots.eyes.on.util.json.JsonSnakeCase
import play.api.libs.json.Reads

case class PostUserForm(
    phoneNumber: String,
    password: String,
    email: Option[String] = None,
    firstName: Option[String] = None,
    lastName: Option[String] = None,
    dobYear: Option[Int] = None,
    dobMonth: Option[Int] = None,
    dobDay: Option[Int] = None,
    gender: Option[String] = None,
    job: Option[String] = None)

object PostUserForm {
  implicit val postUserFormReads: Reads[PostUserForm] = JsonSnakeCase.reads[PostUserForm]
}