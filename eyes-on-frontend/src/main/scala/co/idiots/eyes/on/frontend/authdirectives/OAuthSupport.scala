package co.idiots.eyes.on.frontend.authdirectives

import com.auth0.jwt.JWTVerifier
import play.api.libs.json._
import spray.http.HttpHeaders.Authorization
import spray.http.{HttpHeader, OAuth2BearerToken}
import spray.routing.AuthenticationFailedRejection.{CredentialsMissing, CredentialsRejected}
import spray.routing.{AuthenticationFailedRejection, Directive1, Directives, Rejection}

import scala.collection.JavaConverters._
import scala.util.{Failure, Success, Try}

trait OAuthSupport extends Directives {
  def oauth[T](secret: String)(implicit reads: Reads[T]): Directive1[T] = {
    def extractT(headers: List[HttpHeader]): Either[Rejection, T] = {
      headers.find(_.isInstanceOf[Authorization]) match {
        case Some(header@ Authorization(OAuth2BearerToken(token))) =>
          val tryT = Try {
            val verifier = new JWTVerifier(secret)
            val claims = verifier.verify(token).asScala
              .mapValues(_.asInstanceOf[Any])
              .mapValues {
                case i: Int => JsNumber(i)
                case s: String => JsString(s)
                case _ => JsNull
              }
            Json.toJson(claims).as[T]
          }
          tryT match {
            case Success(t) => Right(t)
            case Failure(_) => Left(AuthenticationFailedRejection(CredentialsRejected, List(header)))
          }
        case Some(header) => Left(AuthenticationFailedRejection(CredentialsRejected, List(header)))
        case None => Left(AuthenticationFailedRejection(CredentialsMissing, List()))
      }
    }
    extract(ctx => extractT(ctx.request.headers)).flatMap {
      case Right(a) => provide(a) & mapRequestContext(ctx => ctx)
      case Left(r) => reject(r)
    }
  }
}
