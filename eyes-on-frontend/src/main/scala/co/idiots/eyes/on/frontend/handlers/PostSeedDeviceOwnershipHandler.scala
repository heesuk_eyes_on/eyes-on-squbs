package co.idiots.eyes.on.frontend.handlers

import akka.actor.ActorSystem
import akka.util.Timeout
import co.idiots.eyes.on.message.{Device, DeviceUUID, Empty}
import co.idiots.eyes.on.util.actorregistry.ActorLookup
import spray.routing.Route

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

trait PostSeedDeviceOwnershipHandler {

  implicit def system: ActorSystem
  implicit def executionContext: ExecutionContext
  implicit def akkaTimeout: Timeout

  def postSeedDeviceOwnership(ownerUserId: Int, hubDevice: DeviceUUID, seedDevice: DeviceUUID, secureCode: String): Route = { ctx =>
    ActorLookup[Empty] ? Device.RegisterEndRequest(ownerUserId, hubDevice, seedDevice, secureCode) onComplete {
      case Success(empty) => ctx.complete(empty)
      case Failure(ex) => ctx.complete(500, ex.getMessage)
    }
  }
}
