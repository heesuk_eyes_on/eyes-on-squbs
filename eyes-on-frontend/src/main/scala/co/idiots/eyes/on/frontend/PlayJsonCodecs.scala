package co.idiots.eyes.on.frontend

import co.idiots.eyes.on.message._
import co.idiots.eyes.on.util.json.JsonSnakeCase
import org.joda.time.{DateTime, DateTimeZone}
import play.api.libs.json.{JsString, Json, Writes}

trait PlayJsonCodecs {

  implicit val userLoginWrites: Writes[UserLogin] = JsonSnakeCase.writes[UserLogin]
  implicit val userWrites: Writes[User] = JsonSnakeCase.writes[User]
  implicit val usersWrites: Writes[Users] = JsonSnakeCase.writes[Users]

  implicit val deviceEventsWrites: Writes[DeviceEvent] = Writes { event =>
    Json.obj(
      "data" -> event.data
    )
  }

  implicit val playJsonWritesForDateTime: Writes[DateTime] = Writes[DateTime](dt => JsString(dt.withZone(DateTimeZone.forID("Asia/Seoul")).toString))

  implicit val journalWithDetectionWrites: Writes[JournalWithDetection] = Writes { jd =>
    Json.obj(
      "journal_id" -> jd.journal.journalId,
      "user_id" -> jd.journal.userId,
      "subject" -> jd.journal.subject,
      "content" -> jd.journal.content,
      "datetime" -> jd.journal.datetime,
      "weather" -> Json.obj(
        "status" -> jd.journal.weatherStatus,
        "daily_max_temp" -> jd.journal.dailyMaxTemp,
        "daily_min_temp" -> jd.journal.dailyMinTemp,
        "daily_humid" -> jd.journal.dailyHumid
      ),
      "detection" -> Json.obj(
        "water_count" -> jd.waterCountAvg,
        "refresh_count" -> jd.refreshCountAvg
      )
    )
  }
  implicit val journalsWithDetectionWrites: Writes[JournalsWithDetection] = JsonSnakeCase.writes[JournalsWithDetection]
  implicit val journalWrites: Writes[Journal] = JsonSnakeCase.writes[Journal]
  implicit val journalsWrites: Writes[Journals] = JsonSnakeCase.writes[Journals]
}
