package co.idiots.eyes.on.frontend.routes

import co.idiots.eyes.on.frontend.forms.{PostDeviceOwnershipForm, PostUserForm}
import co.idiots.eyes.on.frontend.handlers.PostHubDeviceOwnershipHandler
import co.idiots.eyes.on.message.DeviceUUID
import spray.routing.{Directives, Route}

trait PostHubDeviceOwnershipRoute extends Directives with PostHubDeviceOwnershipHandler {

  def postHubDeviceOwnershipRoute: Route = {
    (post & path("users" / IntNumber / "register-hub-device") & entity(as[PostDeviceOwnershipForm])) { (ownerUserId, form) =>
      postHubDeviceOwnership(ownerUserId, DeviceUUID(form.deviceType, form.deviceId), form.secureCode)
    }
  }
}
