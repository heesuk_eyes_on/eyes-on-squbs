package co.idiots.eyes.on.frontend.routes

import co.idiots.eyes.on.frontend.forms.PostUserForm
import co.idiots.eyes.on.frontend.handlers.PostSpecificUserHandler
import spray.routing.{Directives, Route}

trait PostSpecificUserRoute extends Directives with PostSpecificUserHandler {

  def postSpecificUserRoute: Route = {
    (post & path("users") & entity(as[PostUserForm])) { form =>
      postSpecificUser(form)
    }

  }
}