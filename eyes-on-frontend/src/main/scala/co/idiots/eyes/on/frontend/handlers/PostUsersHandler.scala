package co.idiots.eyes.on.frontend.handlers

import akka.actor.ActorSystem
import akka.util.Timeout
import co.idiots.eyes.on.frontend.forms.PostUserForm
import co.idiots.eyes.on.message.{Empty, User}
import co.idiots.eyes.on.util.actorregistry.ActorLookup
import spray.routing.Route

import scala.util.{Failure, Success}
import scala.concurrent.ExecutionContext.Implicits.global

trait PostUsersHandler {

  implicit def system: ActorSystem
  implicit def akkaTimeout: Timeout

  def postUsers(form: PostUserForm): Route = { ctx =>
    ActorLookup[Empty] ? User.CreateRequest(
      form.phoneNumber,
      form.password,
      form.email,
      form.firstName,
      form.lastName,
      form.dobYear,
      form.dobMonth,
      form.dobDay,
      form.gender,
      form.job) onComplete {
      case Success(_) =>
        ctx.complete(Empty())
      case Failure(ex) =>
        ctx.complete(500, ex.getMessage)
    }
  }
}
