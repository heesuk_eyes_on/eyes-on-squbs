package co.idiots.eyes.on.frontend.routes

import co.idiots.eyes.on.frontend.authdirectives.AuthUser
import co.idiots.eyes.on.frontend.handlers.GetUserSpecificJournalHandler
import spray.httpx.PlayJsonSupport
import spray.routing.{Directives, Route}

trait GetUserSpecificJournalRoute extends Directives with AuthUser with PlayJsonSupport with GetUserSpecificJournalHandler {

  def getUserSpecificJournalRoute: Route = {
    (get & path("users" / "self" / "journals" / IntNumber)) { journalId =>
      authInternalUser { userId =>
        getUserSpecificJournal(userId, journalId)
      }
    }
  }
}
