package co.idiots.eyes.on.frontend.routes

import co.idiots.eyes.on.frontend.authdirectives.AuthUser
import co.idiots.eyes.on.frontend.handlers.GetUsersSelfHandler
import spray.routing.{Directives, Route}

trait GetUsersSelfRoute extends Directives with AuthUser with GetUsersSelfHandler {

  def getUsersSelfRoute: Route = {
    (get & path("users" / "self")) {
      authInternalUser { userId =>
        getUsersSelf(userId)
      }
    }
  }
}
