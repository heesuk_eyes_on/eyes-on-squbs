package co.idiots.eyes.on.frontend.routes

import akka.actor.ActorSystem
import akka.util.Timeout
import spray.routing.Route

import scala.concurrent.ExecutionContext

class Routes()(override implicit val system: ActorSystem,
               override val akkaTimeout: Timeout,
               override val executionContext: ExecutionContext)
  extends PostSpecificUserRoute
    with PostUserLoginRoute
    with GetUsersSelfRoute
    with PostHubDeviceOwnershipRoute
    with PostSeedDeviceOwnershipRoute
    with PatchUserSpecificJournalRoute
    with GetUserJournalsRoute
    with GetUserSpecificJournalRoute
    with GetDeviceEventRoute {

  val routeContainer: Route = {
    postSpecificUserRoute ~
    postUserLoginRoute ~
    getUsersSelfRoute ~
    postHubDeviceOwnershipRoute ~
    postSeedDeviceOwnershipRoute ~
    patchUserSpecificJournalRoute ~
    getUserJournalsRoute ~
    getUserSpecificJournalRoute ~
    getDeviceEventRoute
  }
}
