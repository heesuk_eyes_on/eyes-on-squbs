package co.idiots.eyes.on.frontend.handlers

import akka.actor.ActorSystem
import akka.util.Timeout
import co.idiots.eyes.on.message.User
import co.idiots.eyes.on.util.actorregistry.ActorLookup
import spray.routing.Route

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

trait GetUsersSelfHandler {

  implicit def system: ActorSystem
  implicit def akkaTimeout: Timeout
  implicit def executionContext: ExecutionContext

  def getUsersSelf(userId: Int): Route = { ctx =>
    ActorLookup[User] ? User.GetRequest(userId) onComplete {
      case Success(u) => ctx.complete(u)
      case Failure(ex) => ctx.complete(500, ex.getMessage)
    }
  }
}