package co.idiots.eyes.on.frontend.routes

import co.idiots.eyes.on.frontend.forms.PostUserForm
import co.idiots.eyes.on.frontend.handlers.PostUsersHandler
import spray.routing.{Directives, Route}

trait PostUsersRoute extends Directives with PostUsersHandler {

  def postUsersRoute: Route = {
    (post & path("users") & entity(as[PostUserForm])) { form =>
      postUsers(form)
    }
  }
}
