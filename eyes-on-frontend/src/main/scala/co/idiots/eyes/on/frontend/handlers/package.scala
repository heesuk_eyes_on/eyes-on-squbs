package co.idiots.eyes.on.frontend

import spray.httpx.PlayJsonSupport

package object handlers extends PlayJsonCodecs with PlayJsonSupport
