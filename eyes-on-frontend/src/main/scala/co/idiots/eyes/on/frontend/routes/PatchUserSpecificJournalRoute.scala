package co.idiots.eyes.on.frontend.routes

import co.idiots.eyes.on.frontend.authdirectives.AuthUser
import co.idiots.eyes.on.frontend.forms.PatchUserSpecificJournalForm
import play.api.libs.json.Json
import spray.httpx.PlayJsonSupport
import spray.routing.{Directives, Route}

trait PatchUserSpecificJournalRoute extends Directives with AuthUser with PlayJsonSupport {

  def patchUserSpecificJournalRoute: Route = {
    (patch & path("users" / "self" / "journals" / IntNumber) & entity(as[PatchUserSpecificJournalForm])) { (journalId, form) =>
      authInternalUser { userId => ctx =>
        ctx.complete(Json.parse(
          s"""
            |{
            |  "subject": "${form.subject}",
            |  "content": "${form.content}"
            |}
          """.stripMargin))
      }

    }
  }
}
