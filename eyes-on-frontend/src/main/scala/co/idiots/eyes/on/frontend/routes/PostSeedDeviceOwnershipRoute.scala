package co.idiots.eyes.on.frontend.routes

import co.idiots.eyes.on.frontend.authdirectives.AuthDevice
import co.idiots.eyes.on.frontend.forms.PostDeviceOwnershipForm
import co.idiots.eyes.on.frontend.handlers.PostSeedDeviceOwnershipHandler
import co.idiots.eyes.on.message.DeviceUUID
import spray.routing.{Directives, Route}

trait PostSeedDeviceOwnershipRoute extends Directives with PostSeedDeviceOwnershipHandler with AuthDevice {

  def postSeedDeviceOwnershipRoute: Route = {
    (post & path("users" / IntNumber / "register-seed-device") & entity(as[PostDeviceOwnershipForm])) { (ownerUserId, form) =>
      authDevice { case DeviceOAuth(deviceType, deviceId) =>
        val hubDevice = DeviceUUID(deviceType, deviceId)
        val seedDevice = DeviceUUID(form.deviceType, form.deviceId)
        postSeedDeviceOwnership(ownerUserId, hubDevice, seedDevice, form.secureCode)
      }
    }
  }
}
