package co.idiots.eyes.on.frontend.routes

import co.idiots.eyes.on.frontend.forms.PostUserLoginForm
import co.idiots.eyes.on.frontend.handlers.PostUserLoginHandler
import spray.routing.{Directives, Route}

trait PostUserLoginRoute extends Directives with PostUserLoginHandler {

  def postUserLoginRoute: Route = {
    (post & path("users" / "login") & entity(as[PostUserLoginForm])) { form =>
      postUserLogin(form.phoneNumber, form.password)
    }
  }
}