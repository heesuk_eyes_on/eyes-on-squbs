package co.idiots.eyes.on.frontend.authdirectives

import co.idiots.eyes.on.util.json.JsonSnakeCase
import com.typesafe.config.ConfigFactory
import play.api.libs.json.Reads
import spray.routing.Directive1

trait AuthUser extends OAuthSupport {

  private case class UserOAuth(`type`: String, userId: Int)
  private object UserOAuth {
    implicit val reads: Reads[UserOAuth] = JsonSnakeCase.reads[UserOAuth]
  }

  private val userAuthJwtSecret = ConfigFactory.load().getString("api.service.jwt.secret")

  val authInternalUser: Directive1[Int] = oauth[UserOAuth](userAuthJwtSecret).map(_.userId)

}
