package co.idiots.eyes.on.frontend.forms

import co.idiots.eyes.on.util.json.JsonSnakeCase
import play.api.libs.json.Reads

case class PatchUserSpecificJournalForm(subject: String, content: String)
object PatchUserSpecificJournalForm {
  implicit val patchUserSpecificJournalFormReads: Reads[PatchUserSpecificJournalForm] =
    JsonSnakeCase.reads[PatchUserSpecificJournalForm]
}