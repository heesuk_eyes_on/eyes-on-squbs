package co.idiots.eyes.on.frontend.handlers

import akka.actor.ActorSystem
import akka.util.Timeout
import co.idiots.eyes.on.message.{DeviceEvent, DeviceUUID}
import co.idiots.eyes.on.util.actorregistry.ActorLookup
import org.joda.time.DateTime
import spray.routing.Route

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

trait GetDeviceEventHandler {

  implicit def system: ActorSystem
  implicit def akkaTimeout: Timeout

  def getDeviceEvent(deviceUUID: DeviceUUID, from: String, to: String, limit: Int, desc: Boolean): Route = { ctx =>
    ActorLookup[DeviceEvent] ? DeviceEvent.GetRequest(deviceUUID, new DateTime(from), new DateTime(to), limit, desc) onComplete {
      case Success(event) => ctx.complete(event)
      case Failure(ex) => ctx.complete(500, ex.getMessage)
    }
  }
}
