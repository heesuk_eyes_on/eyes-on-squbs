package co.idiots.eyes.on.frontend.forms

import co.idiots.eyes.on.util.json.JsonSnakeCase
import play.api.libs.json.Reads

case class PostUserLoginForm(phoneNumber: String, password: String)

object PostUserLoginForm {
  implicit val postUserLoginFormReads: Reads[PostUserLoginForm] = JsonSnakeCase.reads[PostUserLoginForm]
}
