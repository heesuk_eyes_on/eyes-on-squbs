package co.idiots.eyes.on.frontend.handlers

import akka.actor.ActorSystem
import akka.util.Timeout
import co.idiots.eyes.on.frontend.forms.PostUserForm
import co.idiots.eyes.on.message.{Empty, User}
import co.idiots.eyes.on.util.actorregistry.ActorLookup
import spray.routing.Route

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

trait PostSpecificUserHandler {

  implicit def system: ActorSystem
  implicit def akkaTimeout: Timeout
  implicit def executionContext: ExecutionContext

  def postSpecificUser(form: PostUserForm): Route = { ctx =>
    ActorLookup[Empty] ? User.CreateRequest(form.phoneNumber, form.password) onComplete {
      case Success(_) => ctx.complete(Empty())
      case Failure(ex) => ctx.complete(500, ex.getMessage)
    }
  }
}
