package co.idiots.eyes.on.frontend.authdirectives

import co.idiots.eyes.on.util.json.JsonSnakeCase
import com.typesafe.config.ConfigFactory
import play.api.libs.json.Reads
import spray.routing.Directive1

trait AuthDevice extends OAuthSupport {

  type DeviceType = String
  type DeviceId = Int

  case class DeviceOAuth(`type`: DeviceType, id: DeviceId)
  object DeviceOAuth {
    implicit val reads: Reads[DeviceOAuth] = JsonSnakeCase.reads[DeviceOAuth]
  }

  private val deviceAuthJwtSecret = ConfigFactory.load().getString("api.service.jwt.device.secret")

  val authDevice: Directive1[DeviceOAuth] = oauth[DeviceOAuth](deviceAuthJwtSecret)

}
