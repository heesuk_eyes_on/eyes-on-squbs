package co.idiots.eyes.on.frontend.handlers

import akka.actor.ActorSystem
import akka.util.Timeout
import co.idiots.eyes.on.message._
import co.idiots.eyes.on.util.actorregistry.ActorLookup
import org.joda.time.DateTime
import spray.routing.Route

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

trait GetUserSpecificJournalHandler {

  implicit def system: ActorSystem
  implicit def akkaTimeout: Timeout


  private type WaterCountAvg = Int
  private type RefreshCountAvg = Int

  private def getUserDeviceDetection(userDevice: UserDevice, datetime: DateTime): Future[Detection] =
    ActorLookup[Detection] ? Detection.GetRequest(userDevice, datetime)

  private def getDetectionAvgForAllUserDevice(userDevices: UserDevices, datetime: DateTime): Future[(WaterCountAvg, RefreshCountAvg)] = {
    for {
      allDetections <- Future.sequence(userDevices.data.map(ud => getUserDeviceDetection(ud, datetime)))
      size = userDevices.data.size
      (allWaterCount, allRefreshCount) = allDetections.foldLeft[(WaterCountAvg, RefreshCountAvg)]((0, 0)) { (acc, detection) =>
        (acc._1+ detection.waterCount, acc._2 + detection.refreshCount)
      }
      (waterCountAvg, refreshCountAvg) = (allWaterCount / size, allRefreshCount / size)
    } yield (waterCountAvg, refreshCountAvg)
  }

  def getUserSpecificJournal(userId: Int, journalId: Int): Route = { ctx =>
    val orchF = for {
      journal <- ActorLookup[Journal] ? Journal.GetRequest(journalId)
      userDevices <- ActorLookup[UserDevices] ? UserDevices.GetRequest(userId, "seed")
      (waterCountAvg, refreshCountAvg) <- getDetectionAvgForAllUserDevice(userDevices, journal.datetime)
    } yield JournalWithDetection(journal, waterCountAvg, refreshCountAvg)
    orchF onComplete {
      case Success(journalWithDetection) => ctx.complete(journalWithDetection)
      case Failure(ex) => ctx.complete(ex.getMessage)
    }
  }
}
