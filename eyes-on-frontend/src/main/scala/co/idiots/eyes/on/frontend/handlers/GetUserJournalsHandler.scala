package co.idiots.eyes.on.frontend.handlers

import akka.actor.ActorSystem
import akka.util.Timeout
import co.idiots.eyes.on.message._
import co.idiots.eyes.on.util.actorregistry.ActorLookup
import org.joda.time.{DateTime, DateTimeZone}
import spray.routing.Route

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

trait GetUserJournalsHandler {

  implicit def system: ActorSystem
  implicit def akkaTimeout: Timeout

  private type WaterCountAvg = Int
  private type RefreshCountAvg = Int

  private def getUserDeviceDetection(userDevice: UserDevice, datetime: DateTime): Future[Detection] =
    ActorLookup[Detection] ? Detection.GetRequest(userDevice, datetime)

  private def getDetectionAvgForAllUserDevice(userDevices: UserDevices, datetime: DateTime): Future[(WaterCountAvg, RefreshCountAvg)] = {
    for {
      allDetections <- Future.sequence(userDevices.data.map(ud => getUserDeviceDetection(ud, datetime)))
      size = userDevices.data.size
      (allWaterCount, allRefreshCount) = allDetections.foldLeft[(WaterCountAvg, RefreshCountAvg)]((0, 0)) { (acc, detection) =>
        (acc._1+ detection.waterCount, acc._2 + detection.refreshCount)
      }
      (waterCountAvg, refreshCountAvg) = (allWaterCount / size, allRefreshCount / size)
    } yield (waterCountAvg, refreshCountAvg)
  }

  private def getJournalsWithDetection(userDevices: UserDevices, journals: Journals): Future[Vector[JournalWithDetection]] = {
    for {
      journalsWithDetection <- Future.sequence(journals.data.map { j =>
        getDetectionAvgForAllUserDevice(userDevices, j.datetime).map {
          case (waterCountAvg, refreshCountAvg) => JournalWithDetection(j, waterCountAvg, refreshCountAvg)
        }
      })
    } yield journalsWithDetection
  }

  def getUserJournals(userId: Int, from: String, to: String): Route = { ctx =>
    val orchF = for {
      userDevices <- ActorLookup[UserDevices] ? UserDevices.GetRequest(userId, "seed")
      journals <- ActorLookup[Journals] ? Journals.GetRequest(userId, new DateTime(from).withZone(DateTimeZone.UTC), new DateTime(to).withZone(DateTimeZone.UTC))
      journalsWithDetection <- getJournalsWithDetection(userDevices, journals)
    } yield journalsWithDetection
    orchF onComplete {
      case Success(journals) =>
        ctx.complete(JournalsWithDetection(journals))
      case Failure(ex) => ctx.complete(500, ex.getMessage)
    }
  }
}
