package co.idiots.eyes.on.frontend.authdirectives

/**
  * Created by heesuk on 2017. 5. 26
  */
import com.auth0.jwt.JWTVerifier
import spray.http.{HttpCredentials, HttpHeader, HttpRequest, OAuth2BearerToken}
import spray.routing.RequestContext
import spray.routing.authentication.HttpAuthenticator

import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

case class OAuth(`type`: String, userId: Int)

object OAuth {

  class Authenticator(jwtSecret: String)(implicit ec: ExecutionContext) extends HttpAuthenticator[OAuth] {

    override implicit def executionContext: ExecutionContext = ec

    override def getChallengeHeaders(httpRequest: HttpRequest): List[HttpHeader] = Nil

    override def authenticate(credentials: Option[HttpCredentials], ctx: RequestContext): Future[Option[OAuth]] =
      Future.successful {
        credentials.flatMap {
          case OAuth2BearerToken(token) =>
            Try {
              val verifier = new JWTVerifier(jwtSecret)
              val claims = verifier.verify(token).asScala
              OAuth(
                claims("type").asInstanceOf[String],
                claims("user_id").asInstanceOf[Int])
            }.toOption
          case _ => None
        }
      }
  }
}
