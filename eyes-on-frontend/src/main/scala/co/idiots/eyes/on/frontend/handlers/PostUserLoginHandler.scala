package co.idiots.eyes.on.frontend.handlers

import akka.actor.ActorSystem
import akka.util.Timeout
import co.idiots.eyes.on.message.{Unauthorized, UserLogin}
import co.idiots.eyes.on.util.actorregistry.ActorLookup
import spray.routing.Route

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

trait PostUserLoginHandler {

  implicit def system: ActorSystem
  implicit def akkaTimeout: Timeout
  implicit def executionContext: ExecutionContext

  def postUserLogin(phoneNumber: String, password: String): Route = { ctx =>
    ActorLookup[UserLogin] ? UserLogin.GetRequest(phoneNumber, password) onComplete {
      case Success(loginUserInfo) => ctx.complete(loginUserInfo)
      case Failure(ex: Unauthorized) => ctx.complete(401, ex.getMessage)
      case Failure(ex) => ctx.complete(500, ex.getMessage)
    }
  }

}
