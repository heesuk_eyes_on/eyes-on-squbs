package co.idiots.eyes.on.frontend.handlers

import co.idiots.eyes.on.frontend.RouteTestKit
import co.idiots.eyes.on.message.{Device, DeviceUUID, Empty}
import play.api.libs.json.{JsObject, Json}

import scala.concurrent.ExecutionContext

class PostSeedDeviceOwnershipHandlerSpec extends RouteTestKit with PostSeedDeviceOwnershipHandler {

  override val executionContext: ExecutionContext = system.dispatcher

  val ownerUserId = 1

  val hubDeviceType = "hub"
  val hubDeviceId = 1
  val hubDevice = DeviceUUID(hubDeviceType, hubDeviceId)

  val seedDeviceType = "seed"
  val seedDeviceId = 1
  val seedDevice = DeviceUUID(seedDeviceType, seedDeviceId)

  val secureCode = "seed_device_code"

  private object MockObjects {
    lazy val registerEndDevice: Unit = stub {
      case Device.RegisterEndRequest(`ownerUserId`, `hubDevice`, `seedDevice`, `secureCode`)
        if `ownerUserId` == ownerUserId &&
          `hubDevice` == hubDevice &&
          `seedDevice` == seedDevice &&
          `secureCode` == secureCode =>
        Empty()
    }
  }

  "postSeedDeviceOwnership" should "register seed device to owner user" in {
    MockObjects.registerEndDevice
    Post() ~> postSeedDeviceOwnership(ownerUserId, hubDevice, seedDevice, secureCode) ~> check {
      responseAs[JsObject] shouldBe Json.parse("""{"message":"success"}""")
    }

  }

}
