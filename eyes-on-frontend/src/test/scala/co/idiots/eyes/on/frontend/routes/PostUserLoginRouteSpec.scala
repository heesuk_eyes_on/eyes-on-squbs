package co.idiots.eyes.on.frontend.routes

import co.idiots.eyes.on.frontend.RouteTestKit
import co.idiots.eyes.on.frontend.handlers.PostUserLoginHandler
import play.api.libs.json.Json
import spray.httpx.PlayJsonSupport
import spray.routing.Route

class PostUserLoginRouteSpec extends RouteTestKit with PlayJsonSupport {

  val phoneNumber = "foo"
  val password = "bar"

  trait MockPostUserLoginHandler extends PostUserLoginHandler {
    override def postUserLogin(phoneNumber: String, password: String): Route = {
      ctx => ctx.complete(200)
    }
  }

  val routeContainer: Route = (new Routes() with MockPostUserLoginHandler).routeContainer

  "POST /v1/users/login" should "return user informain if user id and user password corrected" in {
    val json = Json.obj("phone_number" -> phoneNumber, "password" -> password)
    Post("/users/login", json) ~> routeContainer ~> check {
      status.intValue shouldBe 200
    }
  }

}
