package co.idiots.eyes.on.frontend.handlers

import co.idiots.eyes.on.frontend.RouteTestKit
import co.idiots.eyes.on.message._
import org.joda.time.{DateTime, DateTimeZone}
import play.api.libs.json.{JsObject, Json}

class GetUserSpecificJournalHandlerSpec extends RouteTestKit with GetUserSpecificJournalHandler {

  val todayJournalId = 2
  val userId = 1
  val deviceType = "seed"
  val deviceId = 1
  val deviceUUID = DeviceUUID(deviceType, deviceId)
  val defaultSubject = "제목을 입력하세요."
  val defaultContent = "내용을 입력하세요."
  val weatherStatus = "Clear"
  val dailyMaxTemp = 1
  val dailyMinTemp = 1
  val dailyHumid = 1
  val createdAt = new DateTime("2017-11-27T21:50:13.305+09:00")
  val updatedAt = new DateTime("2017-11-27T21:50:13.305+09:00")
  val koreaDateTime: DateTime = new DateTime().withZone(DateTimeZone.forID("Asia/Seoul")).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0)
  val fromDay: DateTime = new DateTime(koreaDateTime).withZone(DateTimeZone.UTC).minusDays(1)
  val toDay: DateTime = new DateTime(koreaDateTime).withZone(DateTimeZone.UTC).plusDays(1)
  val waterCount = 1
  val refreshCount = 1

  val todayJournal = Journal(
    Some(todayJournalId),
    userId,
    defaultSubject,
    defaultContent,
    weatherStatus,
    dailyMaxTemp,
    dailyMinTemp,
    dailyHumid,
    koreaDateTime,
    createdAt,
    updatedAt
  )
  private trait MockObjects {
    lazy val mockGetUserJournal: Unit = addStub {
      case Journal.GetRequest(journalId) if journalId == todayJournalId =>
        todayJournal
    }
    lazy val mockUserDevices: Unit = addStub {
      case UserDevices.GetRequest(_, _) =>
        UserDevices(Vector(UserDevice(userId, deviceUUID)))
    }
    lazy val mockGetDetection: Unit = addStub {
      case Detection.GetRequest(_, _) =>
        Detection(userId, deviceUUID, waterCount, refreshCount, todayJournal.datetime)
    }
  }

  "getUserSpecificJournal" should "return specific journal" in new MockObjects {
    mockGetUserJournal
    mockUserDevices
    mockGetDetection
    Get() ~> getUserSpecificJournal(userId, todayJournalId) ~> check {
      responseAs[JsObject] shouldBe Json.parse(
        s"""
          |{
          |  "journal_id": $todayJournalId,
          |  "user_id": $userId,
          |  "subject": "$defaultSubject",
          |  "content": "$defaultContent",
          |  "datetime": "$koreaDateTime",
          |  "weather": {
          |    "status": "Clear",
          |    "daily_max_temp": 1,
          |    "daily_min_temp": 1,
          |    "daily_humid": 1
          |  },
          |  "detection": {
          |    "water_count": 1,
          |    "refresh_count": 1
          |  }
          |}
        """.stripMargin)
    }
  }
}
