package co.idiots.eyes.on.frontend.handlers

import co.idiots.eyes.on.frontend.RouteTestKit
import co.idiots.eyes.on.message.{Device, DeviceUUID, Empty}
import com.auth0.jwt.JWTSigner
import play.api.libs.json.{JsObject, Json}

import scala.collection.JavaConversions.mapAsJavaMap
import scala.concurrent.ExecutionContext

class PostHubDeviceOwnershipHandlerSpec extends RouteTestKit with PostHubDeviceOwnershipHandler {

  override val executionContext: ExecutionContext = system.dispatcher

  val ownerUserId = 1

  val hubDeviceType = "hub"
  val hubDeviceId = 1
  val hubDevice = DeviceUUID(hubDeviceType, hubDeviceId)
  val secureCode = "code"

  private object MockObjects {
    lazy val registerHubDevice: Unit = stub {
      case Device.RegisterRequest(`ownerUserId`, `hubDevice`, `secureCode`)
        if `ownerUserId` == ownerUserId &&
          `hubDevice` == hubDevice &&
          `secureCode` == secureCode =>
        Empty()
    }
  }

  "postHubDeviceOwnership" should "register hub device to owner user" in {
    MockObjects.registerHubDevice
    Post() ~> postHubDeviceOwnership(ownerUserId, hubDevice, secureCode) ~> check {
      responseAs[JsObject] shouldBe Json.parse(
        s"""
          |{
          |  "device_token": "${getDeviceToken(hubDevice.`type`, hubDevice.id)}"
          |}
        """.stripMargin)
    }
  }

}
