package co.idiots.eyes.on.frontend.routes

import co.idiots.eyes.on.frontend.RouteTestKit
import co.idiots.eyes.on.frontend.handlers.PostSeedDeviceOwnershipHandler
import co.idiots.eyes.on.message.DeviceUUID
import play.api.libs.json.Json
import spray.http.HttpHeaders.Authorization
import spray.http.OAuth2BearerToken
import spray.httpx.PlayJsonSupport
import spray.routing.Route

class PostSeedDeviceOwnershipRouteSpec extends RouteTestKit with PlayJsonSupport {

  trait MockPostSeedDeviceOwnershipHandler extends PostSeedDeviceOwnershipHandler {
    override def postSeedDeviceOwnership(ownerUserId: Int, hubDevice: DeviceUUID, seedDevice: DeviceUUID, secureCode: String): Route = {
      ctx => ctx.complete(200)
    }
  }

  val ownerUserId = 1
  val hubDeviceToken = OAuth2BearerToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0eXBlIjoiaHViIiwiaWQiOjF9.PpU-Aqcvx99DDGyTxFF0hbG-i_B2Tm-93T43-jfpqVo")
  val seedDeviceType = "seed"
  val seedDeviceId = 1
  val secureCode = "seed-secure-code"
  val routes: Route = (new Routes() with MockPostSeedDeviceOwnershipHandler).routeContainer

  "POST /v1/users/:user_id/register-seed-device" should "register seed device to owner user and hub device group" in {
    val json = Json.obj(
      "device_type" -> seedDeviceType,
      "device_id" -> seedDeviceId,
      "secure_code" -> secureCode)
    Post(s"/users/$ownerUserId/register-seed-device", json).withHeaders(Authorization(hubDeviceToken)) ~> routes ~> check {
      status.intValue shouldBe 200
    }
  }

}
