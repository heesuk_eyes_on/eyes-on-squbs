package co.idiots.eyes.on.frontend.routes

import co.idiots.eyes.on.frontend.RouteTestKit
import co.idiots.eyes.on.frontend.forms.PatchUserSpecificJournalForm
import co.idiots.eyes.on.frontend.handlers.PatchUserSpecificJournalHandler
import play.api.libs.json.{JsObject, Json}
import spray.http.HttpHeaders.Authorization
import spray.http.OAuth2BearerToken
import spray.httpx.PlayJsonSupport
import spray.routing.Route

class PatchUserSpecificJournalRouteSpec extends RouteTestKit with PlayJsonSupport {

  val journalId = 1
  val changeSubJect = "change subject name"
  val changeContent = "change content"

  trait MockPatchUserSpecificJournalHandler extends PatchUserSpecificJournalHandler {
    override def patchUserSpecificJournal(form: PatchUserSpecificJournalForm): Route = {
      ctx => ctx.complete(200)
    }
  }

  val routes: Route = (new Routes() with MockPatchUserSpecificJournalHandler).routeContainer

  "Patch /v1/users/self/journals/:journal_id" should "patch specific journal" in {
    val patchJournalForm = Json.obj(
      "subject" -> changeSubJect,
      "content" -> changeContent
    )
    Patch(s"/users/self/journals/$journalId", patchJournalForm).withHeaders(Authorization(userToken)) ~> routes ~> check {
      status.intValue shouldBe 200
    }
  }
}
