package co.idiots.eyes.on.frontend.routes

import co.idiots.eyes.on.frontend.RouteTestKit
import co.idiots.eyes.on.frontend.handlers.PostHubDeviceOwnershipHandler
import co.idiots.eyes.on.message.DeviceUUID
import play.api.libs.json.Json
import spray.httpx.PlayJsonSupport
import spray.routing.Route

class PostHubDeviceOwnershipRouteSpec extends RouteTestKit with PlayJsonSupport {

  val ownerUserId = 1

  val deviceType = "hub"
  val deviceId = 1
  val secureCode = "code"

  trait MockPostHubDeviceOwnershipHandler extends PostHubDeviceOwnershipHandler {
    override def postHubDeviceOwnership(ownerUserId: Int, hubDevice: DeviceUUID, secureCode: String): Route = {
      ctx => ctx.complete(200)
    }
  }

  val routes: Route = (new Routes() with MockPostHubDeviceOwnershipHandler).routeContainer

  "POST /v1/users/:user_id/register-hub-device" should "register hub device for specific user on the device side" in {
    val json = Json.obj(
      "device_type" -> deviceType,
      "device_id" -> deviceId,
      "secure_code" -> secureCode
    )
    Post(s"/users/$ownerUserId/register-hub-device", json) ~> routes ~> check {
      status.intValue shouldBe 200
    }
  }
}
