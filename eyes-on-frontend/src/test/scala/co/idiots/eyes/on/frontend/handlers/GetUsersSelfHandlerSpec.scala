package co.idiots.eyes.on.frontend.handlers

import co.idiots.eyes.on.frontend.RouteTestKit
import co.idiots.eyes.on.message.User
import play.api.libs.json.{JsObject, Json}

import scala.concurrent.ExecutionContext

class GetUsersSelfHandlerSpec extends RouteTestKit with GetUsersSelfHandler {

  override def executionContext: ExecutionContext = system.dispatcher

  val userId = 1
  val phoneNumber = "foo"
  val emailConfirmed = false

  private object MockObjects {
    lazy val getUserSelf: Unit = stub {
      case User.GetRequest(`userId`) if `userId` == userId =>
        User(userId, phoneNumber)
    }
  }

  "getUsersSelf" should "return user's self information" in {
    MockObjects.getUserSelf
    Get() ~> getUsersSelf(userId) ~> check {
      responseAs[JsObject] shouldBe Json.parse(
        s"""
          |{
          |  "id": $userId,
          |  "phone_number": "$phoneNumber",
          |  "email_confirmed": $emailConfirmed
          |}
        """.stripMargin)
    }
  }
}
