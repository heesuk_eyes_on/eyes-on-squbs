package co.idiots.eyes.on.frontend.routes

import co.idiots.eyes.on.frontend.RouteTestKit
import co.idiots.eyes.on.frontend.handlers.GetUserJournalsHandler
import spray.http.HttpHeaders.Authorization
import spray.httpx.PlayJsonSupport
import spray.routing.Route

class GetUserJournalsRouteSpec extends RouteTestKit with PlayJsonSupport {

  trait MockGetUserJournalsHandler extends GetUserJournalsHandler {
    override def getUserJournals(userId: Int, from: String, to: String): Route = {
      ctx => ctx.complete(200)
    }
  }

  val routes: Route = (new Routes() with MockGetUserJournalsHandler).routeContainer

  "GET /v1/users/self/journals" should "return user's journal list" in {
    Get(s"/users/self/journals").withHeaders(Authorization(userToken)) ~> routes ~> check {
      status.intValue shouldBe 200
    }
  }

}
