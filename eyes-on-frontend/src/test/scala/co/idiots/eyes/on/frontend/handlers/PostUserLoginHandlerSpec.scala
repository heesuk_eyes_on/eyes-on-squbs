package co.idiots.eyes.on.frontend.handlers

import co.idiots.eyes.on.frontend.RouteTestKit
import co.idiots.eyes.on.message.{User, UserLogin}
import play.api.libs.json.{JsObject, Json}

import scala.concurrent.ExecutionContext

class PostUserLoginHandlerSpec extends RouteTestKit with PostUserLoginHandler {

  override def executionContext: ExecutionContext = system.dispatcher

  val phoneNumber = "foo"
  val password = "bar"
  val userId = 1
  val accessToken = "token"

  private object MockObjects {
    lazy val returnUserInfo: Unit = stub {
      case UserLogin.GetRequest(`phoneNumber`, `password`)
        if `phoneNumber` == phoneNumber &&
        `password` == password =>
        UserLogin(userId, accessToken)
    }
  }

  "postUserLogin" should "retrieve user login information if user phone_number and password corrected" in {

    MockObjects.returnUserInfo
    Post() ~> postUserLogin(phoneNumber, password) ~> check {
      responseAs[JsObject] shouldBe Json.parse(
        s"""
          |{
          |  "user_id": $userId,
          |  "access_token": "$accessToken"
          |}
        """.stripMargin)
    }
  }

}
