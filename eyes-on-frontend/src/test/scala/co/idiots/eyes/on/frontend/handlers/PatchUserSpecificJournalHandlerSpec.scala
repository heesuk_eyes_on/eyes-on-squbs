package co.idiots.eyes.on.frontend.handlers

import co.idiots.eyes.on.frontend.RouteTestKit
import co.idiots.eyes.on.frontend.forms.PatchUserSpecificJournalForm

class PatchUserSpecificJournalHandlerSpec extends RouteTestKit with PatchUserSpecificJournalHandler {

  "patchUserSpecificJournal" should "patch user specific journal item" in {
    val changeSubejct = "heesuk`s 농사일지"
    val defaultContent = "change content"
    val form = PatchUserSpecificJournalForm(changeSubejct, defaultContent)

    Patch() ~> patchUserSpecificJournal(form)
  }
}
