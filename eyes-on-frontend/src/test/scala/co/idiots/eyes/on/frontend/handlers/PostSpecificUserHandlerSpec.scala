package co.idiots.eyes.on.frontend.handlers

import co.idiots.eyes.on.frontend.forms.PostUserForm
import co.idiots.eyes.on.frontend.RouteTestKit
import co.idiots.eyes.on.message.{Empty, User}
import play.api.libs.json.{JsObject, Json}

import scala.concurrent.ExecutionContext

class PostSpecificUserHandlerSpec extends RouteTestKit with PostSpecificUserHandler {

  override def executionContext: ExecutionContext = system.dispatcher

  val createdId = 1
  val phoneNumber = "user_id"
  val password = "password"

  private object MockObejcts {
    lazy val createUser: Unit = stub {
      case User.CreateRequest(`phoneNumber`, `password`, _, _, _, _, _, _, _, _)
        if `phoneNumber` == phoneNumber &&
          `password` == password => Empty()
    }
  }

  "postSpecificUser" should "create new user" in {
    val form = PostUserForm(phoneNumber, password)

    MockObejcts.createUser
    Post() ~> postSpecificUser(form) ~> check {
      responseAs[JsObject] shouldBe Json.parse("""{"message": "success"}""")
    }
  }
}
