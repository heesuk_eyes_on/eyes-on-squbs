package co.idiots.eyes.on.frontend.handlers

import co.idiots.eyes.on.frontend.RouteTestKit
import co.idiots.eyes.on.message._
import org.joda.time.{DateTime, DateTimeZone}
import play.api.libs.json.{JsObject, Json}

class GetUserJournalsHandlerSpec extends RouteTestKit with GetUserJournalsHandler {

  val yesterDayJournalId = 1
  val todayJournalId = 2
  val userId = 1
  val defaultSubject = "제목을 입력하세요."
  val defaultContent = "내용을 입력하세요."
  val weatherStatus = "Clear"
  val dailyMaxTemp = 1
  val dailyMinTemp = 1
  val dailyHumid = 1
  val createdAt = new DateTime("2017-11-27T21:50:13.305+09:00")
  val updatedAt = new DateTime("2017-11-27T21:50:13.305+09:00")
  val koreaDateTime: DateTime = new DateTime().withZone(DateTimeZone.forID("Asia/Seoul")).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0)
  val fromDay: DateTime = new DateTime(koreaDateTime).withZone(DateTimeZone.UTC).minusDays(1)
  val toDay: DateTime = new DateTime(koreaDateTime).withZone(DateTimeZone.UTC).plusDays(1)
  val deviceType = "seed"
  val deviceId = 1
  val deviceUUID = DeviceUUID(deviceType, deviceId)
  val userDevice = UserDevice(userId, deviceUUID)

  val yesterDayJournal = Journal(
    Some(yesterDayJournalId),
    userId,
    defaultSubject,
    defaultContent,
    weatherStatus,
    dailyMaxTemp,
    dailyMinTemp,
    dailyHumid,
    koreaDateTime.minusDays(1),
    createdAt,
    updatedAt
  )
  val todayJournal = Journal(
    Some(todayJournalId),
    userId,
    defaultSubject,
    defaultContent,
    weatherStatus,
    dailyMaxTemp,
    dailyMinTemp,
    dailyHumid,
    koreaDateTime,
    createdAt,
    updatedAt
  )

  private trait MockObjects {
    lazy val mockGetUserJournals: Unit = addStub {
      case Journals.GetRequest(`userId`, _, _) if `userId` == userId =>
        Journals(Vector(todayJournal, yesterDayJournal))
    }
    lazy val mockGetUserDevices: Unit = addStub {
      case UserDevices.GetRequest(`userId`, _) if `userId` == userId =>
        UserDevices(Vector(userDevice))
    }
    lazy val mockGetDetectionTodayForDevice: Unit = addStub {
      case Detection.GetRequest(`userDevice`, datetime) if `userDevice` == userDevice && datetime == todayJournal.datetime =>
        Detection(userId, deviceUUID, 1, 1, todayJournal.datetime)
    }
    lazy val mockGetDetectionYesterDayForDevice: Unit = addStub {
      case Detection.GetRequest(`userDevice`, datetime) if `userDevice` == userDevice && datetime == yesterDayJournal.datetime =>
        Detection(userId, deviceUUID, 1, 1, yesterDayJournal.datetime)
    }
  }

  "getUserJournalsHandler" should "return user's journal list" in new MockObjects {
    mockGetUserJournals
    mockGetUserDevices
    mockGetDetectionTodayForDevice
    mockGetDetectionYesterDayForDevice

    Get() ~> getUserJournals(userId, fromDay.toString, toDay.toString) ~> check {
      responseAs[JsObject] shouldBe Json.parse(
        s"""
          |{
          |  "data": [
          |    {
          |      "journal_id": $todayJournalId,
          |      "user_id": $userId,
          |      "subject": "$defaultSubject",
          |      "content": "$defaultContent",
          |      "datetime": "$koreaDateTime",
          |      "weather": {
          |        "status": "Clear",
          |        "daily_max_temp": 1,
          |        "daily_min_temp": 1,
          |        "daily_humid": 1
          |      },
          |      "detection": {
          |        "water_count": 1,
          |        "refresh_count": 1
          |      }
          |    },
          |    {
          |      "journal_id": $yesterDayJournalId,
          |      "user_id": $userId,
          |      "subject": "$defaultSubject",
          |      "content": "$defaultContent",
          |      "datetime": "${koreaDateTime.minusDays(1)}",
          |      "weather": {
          |        "status": "Clear",
          |        "daily_max_temp": 1,
          |        "daily_min_temp": 1,
          |        "daily_humid": 1
          |      },
          |      "detection": {
          |        "water_count": 1,
          |        "refresh_count": 1
          |      }
          |    }
          |  ]
          |}
        """.stripMargin)
    }
  }
}