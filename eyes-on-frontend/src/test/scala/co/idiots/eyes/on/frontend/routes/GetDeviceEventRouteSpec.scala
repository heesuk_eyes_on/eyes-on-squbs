package co.idiots.eyes.on.frontend.routes

import co.idiots.eyes.on.frontend.RouteTestKit
import co.idiots.eyes.on.frontend.authdirectives.AuthUser
import co.idiots.eyes.on.frontend.handlers.GetDeviceEventHandler
import co.idiots.eyes.on.message.DeviceUUID
import spray.http.HttpHeaders.Authorization
import spray.httpx.PlayJsonSupport
import spray.routing.Route

class GetDeviceEventRouteSpec extends RouteTestKit with AuthUser with PlayJsonSupport {

  val deviceType = "seed"
  val deviceId = 1

  private trait MockGetDeviceEventHandler extends GetDeviceEventHandler {
    override def getDeviceEvent(deviceUUID: DeviceUUID,
                                from: String,
                                to: String,
                                limit: Int,
                                desc: Boolean): Route = { ctx =>
      ctx.complete(200)
    }
  }

  val routes: Route = (new Routes() with MockGetDeviceEventHandler).routeContainer

  "getDeviceEvent" should "return device event" in {
    Get(s"/users/self/devices/$deviceType/$deviceId/events/raw-data").withHeaders(Authorization(userToken)) ~> routes ~> check {
      status.intValue shouldBe 200
    }
  }

}