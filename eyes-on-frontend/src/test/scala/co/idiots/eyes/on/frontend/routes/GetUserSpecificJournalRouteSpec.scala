package co.idiots.eyes.on.frontend.routes

import co.idiots.eyes.on.frontend.RouteTestKit
import co.idiots.eyes.on.frontend.handlers.GetUserSpecificJournalHandler
import spray.http.HttpHeaders.Authorization
import spray.httpx.PlayJsonSupport
import spray.routing.Route

class GetUserSpecificJournalRouteSpec extends RouteTestKit with PlayJsonSupport {


  trait MockGetUserSpecificJournalHandler extends GetUserSpecificJournalHandler {
    override def getUserSpecificJournal(userId: Int, journalId: Int): Route = {
      ctx => ctx.complete(200)
    }
  }

  val routes: Route = (new Routes() with MockGetUserSpecificJournalHandler).routeContainer
  val journalId = 1

  "GET /v1/users/self/journals/:journal_id" should "return specific journal" in {
    Get(s"/users/self/journals/$journalId").withHeaders(Authorization(userToken)) ~> routes ~> check {
      status.intValue shouldBe 200
    }
  }

}
