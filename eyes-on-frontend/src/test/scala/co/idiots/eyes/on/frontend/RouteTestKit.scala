package co.idiots.eyes.on.frontend

import akka.util.Timeout
import co.idiots.eyes.on.util.TestKit
import org.scalatest.{FlatSpecLike, Matchers}
import spray.http.OAuth2BearerToken

import scala.concurrent.duration._

abstract class RouteTestKit extends TestKit(false) with FlatSpecLike with Matchers {

  val userToken = OAuth2BearerToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0eXBlIjoidXNlciIsInVzZXJfaWQiOjF9.raKdboxTWu35zR5l6hQPn8YpwKHNEoP4t4mwmZxspJg")

  implicit val akkaTimeout = Timeout(3.seconds)
}


