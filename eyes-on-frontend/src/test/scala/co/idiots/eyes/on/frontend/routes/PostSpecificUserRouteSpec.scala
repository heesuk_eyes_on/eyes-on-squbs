package co.idiots.eyes.on.frontend.routes

import co.idiots.eyes.on.frontend.RouteTestKit
import co.idiots.eyes.on.frontend.forms.PostUserForm
import co.idiots.eyes.on.frontend.handlers.PostSpecificUserHandler
import play.api.libs.json.Json
import spray.httpx.PlayJsonSupport
import spray.routing.Route

class PostSpecificUserRouteSpec extends RouteTestKit with PlayJsonSupport {

  trait MockPostSpecificUserHandler extends PostSpecificUserHandler {
    override def postSpecificUser(form: PostUserForm): Route = {
      ctx => ctx.complete(200)
    }
  }

  val phoneNumber = "test-id"
  val password = "test-password"
  val routeContainer: Route = (new Routes() with MockPostSpecificUserHandler).routeContainer

  "POST /v1/users" should "create new user" in {
    val json = Json.obj("phone_number" -> phoneNumber, "password" -> password)
    Post("/users", json) ~> routeContainer ~> check {
      status.intValue shouldBe 200
    }
  }

}
