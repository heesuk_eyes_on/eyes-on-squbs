package co.idiots.eyes.on.frontend.routes

import co.idiots.eyes.on.frontend.RouteTestKit
import co.idiots.eyes.on.frontend.handlers.GetUsersSelfHandler
import spray.http.HttpHeaders.Authorization
import spray.httpx.PlayJsonSupport
import spray.routing.Route

class GetUsersSelfRouteSpec extends RouteTestKit with PlayJsonSupport {

  trait MockGetUsersSelfHandler extends GetUsersSelfHandler {
    override def getUsersSelf(userId: Int): Route = {
      ctx => ctx.complete(200)
    }
  }

  val routes: Route = (new Routes with MockGetUsersSelfHandler).routeContainer

  "GET /v1/users/self" should "return user's self information" in {
    Get("/users/self").withHeaders(Authorization(userToken)) ~> routes ~> check {
      status.intValue shouldBe 200
    }
  }
}
