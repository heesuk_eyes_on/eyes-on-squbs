package co.idiots.eyes.on.frontend.handlers

import co.idiots.eyes.on.frontend.RouteTestKit
import co.idiots.eyes.on.message.{DeviceEvent, DeviceUUID}
import org.joda.time.DateTime
import play.api.libs.json.{JsArray, JsObject, JsValue, Json}

class GetDeviceEventHandlerSpec extends RouteTestKit with GetDeviceEventHandler {

  val deviceType = "seed"
  val deviceId = 1
  val deviceUUID = DeviceUUID(deviceType, deviceId)

  val from = new DateTime()
  val to = new DateTime()
  val limit = 1
  val desc = false
  val soilValue = 30
  val tempValue = 30
  val humidValue = 30
  val timestamp = new DateTime("2017-11-25T00:00:00.000Z").toString

  val eventJs: JsObject = Json.obj(
    "device_type" -> deviceType,
    "device_id" -> deviceId,
    "sensor_value" -> Json.obj(
      "soil" -> soilValue,
      "temp" -> tempValue,
      "humid" -> humidValue
    ),
    "detection" -> Json.obj(
      "water_detection" -> true,
      "refresh_detection" -> false
    ),
    "timestamp" -> timestamp
  )
  val data = JsArray(Vector(eventJs))

  private trait MockObjects {
    lazy val mockGetDeviceEvent: Unit = stub {
      case DeviceEvent.GetRequest(`deviceUUID`, `from`, `to`, `limit`, `desc`)
        if `deviceUUID` == deviceUUID &&
        `from` == from &&
        `to` == to &&
        `limit` == limit &&
        `desc` == desc =>
        DeviceEvent(data)
    }
  }

  "getDeviceEvent" should "return specific device event raw-data" in new MockObjects {
    mockGetDeviceEvent
    Get() ~> getDeviceEvent(deviceUUID, from.toString, to.toString, limit, desc) ~> check {
      responseAs[JsObject] shouldBe Json.parse(
        s"""
          |{
          |  "data": [
          |    {
          |      "device_type": "$deviceType",
          |      "device_id": $deviceId,
          |      "sensor_value": {
          |        "soil": $soilValue,
          |        "temp": $tempValue,
          |        "humid": $humidValue
          |      },
          |      "detection": {
          |        "water_detection": true,
          |        "refresh_detection": false
          |      },
          |      "timestamp": "$timestamp"
          |    }
          |  ]
          |}
        """.stripMargin)
    }
  }
}
