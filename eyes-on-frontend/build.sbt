import Versions._

libraryDependencies ++= Seq(
  "com.github.t3hnar" %% "scala-bcrypt" % "3.0",
"com.typesafe.play" %% "play-json" % "2.5.9",
  "org.fusesource.mqtt-client" % "mqtt-client" % "1.12",
  "org.squbs" %% "squbs-testkit" % squbs % "test",
  "org.squbs" %% "squbs-unicomplex" % squbs,
  "io.spray" % "spray-http_2.11" % "1.3.4")