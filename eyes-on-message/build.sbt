import Versions._

libraryDependencies ++= Seq(
  "com.typesafe.play" %% "play-json" % "2.5.9",
  "org.fusesource.mqtt-client" % "mqtt-client" % "1.12",
  "org.squbs" %% "squbs-testkit" % squbs % "test")