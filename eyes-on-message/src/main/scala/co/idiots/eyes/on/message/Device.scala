package co.idiots.eyes.on.message

case class Device(deviceUUID: DeviceUUID, deviceName: Option[String]) extends Response
object Device {
  case class RegisterRequest(ownerUserId: Int, hubDevice: DeviceUUID, secureCode: String)
  case class RegisterEndRequest(ownerUserId: Int, hubDevice: DeviceUUID, seedDevice: DeviceUUID, secureCode: String)
}

case class DeviceUUID(`type`: String, id: Int)