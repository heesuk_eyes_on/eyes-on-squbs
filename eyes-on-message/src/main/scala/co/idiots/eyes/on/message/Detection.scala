package co.idiots.eyes.on.message

import org.joda.time.DateTime

case class Detection(userId: Int, deviceUUID: DeviceUUID, waterCount: Int, refreshCount: Int, timestamp: DateTime) extends Response
object Detection {
  case class CreateRequest(userId: Int, deviceUUID: DeviceUUID, datetime: DateTime, detected: IsDetected)
  case class GetRequest(userDevice: UserDevice, datetime: DateTime)
}
