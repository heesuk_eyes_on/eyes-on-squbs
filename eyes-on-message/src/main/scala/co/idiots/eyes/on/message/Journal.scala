package co.idiots.eyes.on.message

import org.joda.time.DateTime

case class Journal(
    journalId: Option[Int],
    userId: Int,
    subject: String,
    content: String,
    weatherStatus: String,
    dailyMaxTemp: Double,
    dailyMinTemp: Double,
    dailyHumid: Double,
    datetime: DateTime,
    createdAt: DateTime = new DateTime(),
    updatedAt: DateTime = new DateTime()) extends Response

object Journal {
  case object CreateAllUserDailyJournal
  case class GetRequest(journalId: Int)
}

case class Journals(data: Vector[Journal]) extends Response
object Journals {
  case class GetRequest(userId: Int, from: DateTime, to: DateTime)
}

case class JournalWithDetection(journal: Journal, waterCountAvg: Int, refreshCountAvg: Int)
case class JournalsWithDetection(data: Vector[JournalWithDetection])
