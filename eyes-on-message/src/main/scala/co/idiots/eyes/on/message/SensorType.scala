package co.idiots.eyes.on.message
/**
  * Created by heesuk on 2017. 5. 7..
  */

trait SensorType
case object Soil extends SensorType
case object Temp extends SensorType
case object Humid extends SensorType
