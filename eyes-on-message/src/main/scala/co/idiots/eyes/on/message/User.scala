package co.idiots.eyes.on.message

/**
  * Created by heesuk on 2017. 5. 22..
  */
case class User(
    id: Int,
    phoneNumber: String,
    email: Option[String] = None,
    emailConfirmed: Option[Boolean] = Some(false),
    emailCode: Option[String] = None,
    firstName: Option[String] = None,
    lastName: Option[String] = None,
    dobYear: Option[Int] = None,
    dobMonth: Option[Int]= None,
    dobDay: Option[Int] = None,
    gender: Option[String] = None,
    job: Option[String] = None) extends Response

object User {
  case class CreateRequest(
      phoneNumber: String,
      password: String,
      email: Option[String] = None,
      firstName: Option[String] = None,
      lastName: Option[String] = None,
      dobYear: Option[Int] = None,
      dobMonth: Option[Int] = None,
      dobDay: Option[Int] = None,
      gender: Option[String] = None,
      job: Option[String]= None)
  case class GetRequest(userId: Int)
}

case class Users(data: Vector[User]) extends Response

object Users {
  case class GetRequest()
}

case class UserLogin(userId: Int, accessToken: String) extends Response
object UserLogin {
  case class GetRequest(phoneNumber: String, password: String)
}

