package co.idiots.eyes.on.message

import com.fasterxml.jackson.annotation.JsonValue
import org.joda.time.DateTime
import play.api.libs.json.{JsArray, JsObject}

/**
  * Created by heesuk on 2017. 5. 1..
  */
case class DeviceEvent(data: JsArray) extends Response

object DeviceEvent {
  case class GetRequest(deviceUUID: DeviceUUID, from: DateTime, to: DateTime, limit: Int, desc: Boolean)
  case class CreateRequest(deviceUUID: DeviceUUID, payload: Payload)
}

case class DeviceSensorComponent(data: Map[SensorType, Double])