package co.idiots.eyes.on.message

case class SensorValue(soil: Double, temp: Double, humid: Double)
case class IsDetected(waterDetection: Boolean, refreshDetection: Boolean)
case class Payload(deviceUUID: DeviceUUID, sensorValue: SensorValue, detection: IsDetected)