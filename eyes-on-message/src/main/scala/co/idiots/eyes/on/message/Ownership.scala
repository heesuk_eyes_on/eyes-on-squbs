package co.idiots.eyes.on.message

case class Ownership(
    userId: Int,
    deviceUUID: DeviceUUID,
    ownerType: String) extends Response

object Ownership {
  case class GetRequest(deviceUUID: DeviceUUID)
}