package co.idiots.eyes.on.message

case class Weather(
    dailyMaxTemp: Double,
    dailyMinTemp: Double,
    dailyHumid: Double,
    datetime: Long,
    status: String) extends Response

object Weather {
  case class GetCurrentDailyWeatherRequest()
}