package co.idiots.eyes.on.message

case class UserDevice(userId: Int, deviceUUID: DeviceUUID)

case class UserDevices(data: Vector[UserDevice]) extends Response
object UserDevices {
  case class GetRequest(userId: Int, deviceType: String)
}
