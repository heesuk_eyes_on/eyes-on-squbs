package co.idiots.eyes.on.message

import play.api.libs.json.{Json, Writes}

trait Response

abstract class ErrorResponse(message: String) extends Throwable(message) with Response {
  override def toString: String = s"${getClass.getSimpleName}($message)"
}
case class BadRequest(message: String) extends ErrorResponse(message)
case class Unauthorized(message: String) extends ErrorResponse(message)
case class Unprocessable(message: String) extends ErrorResponse(message)
case class NotFound(message: String) extends ErrorResponse(message)
case class InternalError(message: String) extends ErrorResponse(message)

case class Empty() extends Response
object Empty {
  implicit val writes: Writes[Empty] = Writes { _ =>
    Json.obj("message" -> "success")
  }
}
