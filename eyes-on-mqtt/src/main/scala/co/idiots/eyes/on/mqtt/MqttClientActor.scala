package co.idiots.eyes.on.mqtt
/**
  * Created by heesuk on 2017. 4. 10..
  */
import java.util.UUID

import akka.actor.{Actor, ActorLogging}
import akka.util.Timeout
import co.idiots.eyes.on.message._
import co.idiots.eyes.on.util.actorregistry.ActorLookup
import com.typesafe.config.ConfigFactory
import org.fusesource.hawtbuf.{Buffer, UTF8Buffer}
import org.fusesource.mqtt.client._
import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormatter, ISODateTimeFormat}
import play.api.libs.json._

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success}

private[this] class MqttClientActor extends Actor with ActorLogging {
  import context.dispatcher

  implicit val timeout = Timeout(3.seconds)

  val parser: DateTimeFormatter = ISODateTimeFormat.dateTime()
  private val mqttTopics = "#"
  private val conf = ConfigFactory.load.getConfig("mqtt")
  private val client = new MQTT

  client.setHost(conf.getString("url"))
  client.setClientId("mqtt-client-actor" + UUID.randomUUID())
  client.setUserName(conf.getString("username"))
  client.setPassword(conf.getString("password"))
  client.setVersion("3.1.1")
  client.setReconnectAttemptsMax(-1)
  client.setReconnectDelay(100)

  private val connection = client.callbackConnection()

  connection.listener(new Listener {
    override def onConnected(): Unit = {
      self ! StartSubscribe
      log.info("success mqtt connection.")
    }
    override def onFailure(value: Throwable): Unit = {
      log.info("suddenly happen to failure.")
    }
    override def onDisconnected(): Unit = {
      log.info("successfully disconnected.")
    }
    override def onPublish(topic: UTF8Buffer, body: Buffer, ack: Runnable): Unit = {
      log.info("mqtt data subscribe.")
      val mqttTopicO = Topic.deviceUUIDFromMqttTopic(topic.toString)
      self ! MqttMsg(mqttTopicO, Json.parse(body.toByteArray))
    }
  })

  connection.connect(new Callback[Void] {
    override def onSuccess(value: Void): Unit = {
    }
    override def onFailure(th: Throwable): Unit = {
      log.info("connect fail!")
    }
  })

  private def convertToPayload(payloadJs: JsValue): JsResult[Payload] = {
    for {
      deviceType <- (payloadJs \ "device_type").validateOpt[String] flatMap {
        case Some(deviceType) => JsSuccess(deviceType)
        case None => JsError("Js Parsing Error about `device_type` in mqtt message")
      }
      deviceId <- (payloadJs \ "device_id").validateOpt[Int] flatMap {
        case Some(deviceId) => JsSuccess(deviceId.toInt)
        case None => JsError("Js Parsing Error about `device_id` in mqtt message")
      }
      soilRaw <- (payloadJs \ "sensor_value" \ "soil").validateOpt[Double] flatMap {
        case Some(soilRaw) => JsSuccess(soilRaw)
        case None => JsError("Js Parsing Error about `soil` sensor value in mqtt message")
      }
      tempRaw <- (payloadJs \ "sensor_value" \ "temp").validateOpt[Double] flatMap {
        case Some(tempRaw) => JsSuccess(tempRaw)
        case None => JsError("Js Parsing Error about `temp` sensor value in mqtt message")
      }
      humidRaw <- (payloadJs \ "sensor_value" \ "humid").validateOpt[Double] flatMap {
        case Some(humidRaw) => JsSuccess(humidRaw)
        case None => JsError("Js Parsing Error about `humid` sensor value in mqtt message")
      }
      isWaterDetection <- (payloadJs \ "detection" \ "water_detection").validateOpt[Boolean] flatMap {
        case Some(isWaterDetection) => JsSuccess(isWaterDetection)
        case None => JsError("Js Parsing Error about `water_detection` in mqtt message")
      }
      isRefreshDetection <- (payloadJs \ "detection" \ "refresh_detection").validateOpt[Boolean] flatMap {
        case Some(isRefreshDetection) => JsSuccess(isRefreshDetection)
        case None => JsError("Js Parsing Error about `refresh_detection` in mqtt message")
      }
    } yield Payload(DeviceUUID(deviceType, deviceId), SensorValue(soilRaw, tempRaw, humidRaw), IsDetected(isWaterDetection, isRefreshDetection))
  }

  override def receive: Receive = {
    case StartSubscribe =>
      connection.subscribe(Array(new Topic(mqttTopics, QoS.AT_MOST_ONCE)),
        new Callback[Array[Byte]] {
          override def onSuccess(value: Array[Byte]): Unit = {
            log.info("subscribe connection Success")
          }
          override def onFailure(value: Throwable): Unit = {
            log.info("subscribe connection Failed")
          }
        })

    case MqttMsg(deviceUUIDO, payloadJs) =>
      log.info("received subscription message")
      deviceUUIDO match {
        case Some(deviceUUID) =>
          val orchF = for {
            payload <- Future { convertToPayload(payloadJs) }
            _ <- Future { println("payload received! " + payload) }
            _ <- ActorLookup[Empty] ? DeviceEvent.CreateRequest(deviceUUID, payload.get)
            _ <- Future { println("DeviceEvent! " + payload) }
            deviceOwnerUserId <- (ActorLookup[Ownership] ? Ownership.GetRequest(deviceUUID)).map(_.userId)
            _ <- Future { println("deviceOwnerUserId! " + deviceOwnerUserId) }
            _ <- payload.get.detection match {
              case IsDetected(false, false) => Future.successful()
              case _ => ActorLookup[Empty] ? Detection.CreateRequest(deviceOwnerUserId, deviceUUID, new DateTime(), payload.get.detection)
            }
            _ <- Future { println("isDetected? " + payload.get.detection) }
          } yield ()

          orchF onComplete {
            case Success(_) => log.info("success to send DeviceEvent.CreateRequest")
            case Failure(ex) =>
          }
        case None => log.error("invalid topic")
      }
  }
}