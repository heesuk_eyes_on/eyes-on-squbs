package co.idiots.eyes.on

import co.idiots.eyes.on.message.DeviceUUID
import play.api.libs.json.JsValue

package object mqtt {

  case object StartSubscribe
  case class MqttMsg(deviceUUID: Option[DeviceUUID], payload: JsValue)

  sealed trait MqttTopic
  case class Device(deviceType: String, deviceId: Int) extends MqttTopic

  object Topic {
    def deviceUUIDFromMqttTopic(topic: String): Option[DeviceUUID] = {
      topic.split("/").toList match {
        case "devices" :: deviceType :: deviceId :: "events" :: "raw-data" :: Nil =>
          Some(DeviceUUID(deviceType, deviceId.toInt))
        case _ =>
          None
      }
    }
  }
}