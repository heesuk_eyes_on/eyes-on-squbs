import Versions._

name := "eyes-on-squbs"

version := "latest"

organization in ThisBuild := "co.idiot"

scalaVersion := "2.11.8"

scalaVersion in ThisBuild := "2.11.8"

parallelExecution in Test := false

publishArtifact in root := false

fork in Test := false

val quillVersion = "1.3.0"

lazy val `eyes-on-frontend` = project.dependsOn(`eyes-on-message`, `eyes-on-util`)
lazy val `eyes-on-backend` = project.dependsOn(`eyes-on-message`, `eyes-on-util`)
lazy val `eyes-on-message` = project
lazy val `eyes-on-util` = project.dependsOn(`eyes-on-message`)
lazy val `eyes-on-mqtt` = project.dependsOn(`eyes-on-message`, `eyes-on-util`)
//각각의 멀티 모듈 프로젝트를 생성한다
//lazy val root = project.in(file("."))
//  .dependsOn(`eyes-on-message`, `eyes-on-backend`, `eyes-on-frontend`, `eyes-on-mqtt`)
//  .aggregate(`eyes-on-message`, `eyes-on-backend`, `eyes-on-frontend`, `eyes-on-mqtt`)//각각의 멀티모듈 프로젝트를 하나의 프로젝트로 병합하기 위해서 aggregate 설정을 해준다.

lazy val root = project.in(file("."))
  .dependsOn(`eyes-on-message`, `eyes-on-util`, `eyes-on-mqtt`, `eyes-on-backend`, `eyes-on-frontend`)
  .aggregate(`eyes-on-message`, `eyes-on-util`,`eyes-on-backend`, `eyes-on-frontend`, `eyes-on-mqtt`)

mainClass in Compile := Some("org.squbs.unicomplex.Bootstrap")
/*
  최초의 진입점이 되는 메인클래스가 무엇인지 설정한다.
  우리는 최초의 진입점을 org.squbs.unicomplex.Bootstrap을 설정한다
  이 Bootstrap은 각 프로젝트의 resource에서 meta파일을 보는데 그 메타파일이 actor message인지, service인지를 확인하고
  Actor system에 등록한다. 여기서 만약 액터일 경우에는 'Well Known'액터가 되는데, 액터의 주소를 몰라도 액터의 메세지를 안다면 호출할 수 있게 된다.
 */

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akka,
  "com.typesafe.akka" %% "akka-slf4j" % akka,
  "org.squbs" %% "squbs-unicomplex" % "0.8.1",
  "io.getquill" %% "quill-core" % quillVersion,
  "io.getquill" %% "quill-jdbc" % quillVersion)

enablePlugins(JavaAppPackaging)